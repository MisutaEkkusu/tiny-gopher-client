CFLAGS0 = -Wall -Wextra
SRCDIR  = src_lnx
BINDIR  = bin

all:
	gcc $(CFLAGS0)\
		$(addprefix $(SRCDIR)/,\
		tinygopher.c\
		tinytypes.c\
		tinystate.c\
		tinyparse.c\
		tinyfetch.c\
		tinydisp.c\
		tinydraw.c\
		tinyrender.c\
		tinysocket.c\
		tinyhist.c\
		tinycache.c\
		tinyconfig.c\
		tinystring.c\
		tinydebug.c\
		tinydebug_types.c\
		)\
		-o $(BINDIR)/tinygopher

debug:
	gcc $(CFLAGS)\
		$(SRCDIR)/tinygopher-debug-monitor.c\
		$(SRCDIR)/tinydebug.c\
		-o $(BINDIR)/tinygopher-debug-monitor
