// Gopher parser and display
// V2 (2018-10-24)

// Lê um ficheiro do stdin, faz parsing e retorna o print da linha

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<inttypes.h>

#include "../../src/tinytypes.h"

int main(){
	//size_t buffer_len = 4096;
	size_t buffer_len = 0;
	char* buffer = NULL;
	
	GOPH_line_t line_data;

	int8_t ret = 0;
	
	while(1) {
		getline(&buffer, &buffer_len, stdin);

		if (feof(stdin)){
			exit(0);
		}

		
		//if (GOPH_line_parser(buffer, buffer_len, &line_data)) break;
		ret = GOPH_line_parser(buffer, buffer_len, &line_data);

		// DESESPERO
		//free(buffer);
		//buffer_len = 0;
		
		printf("\n\n");
		puts(buffer);
		printf("parser ret: %d\n", ret);
		GOPH_line_print(&line_data);
		GOPH_line_reset(&line_data);
	}

	return 0;
}
