#!/usr/bin/python3

import subprocess as sp

def getMem(N,g):
	p = sp.run(["/usr/bin/time", "-f", "%M", "bin/size2", str(N), str(g)],
	           stdout=sp.PIPE, stderr = sp.PIPE)
	# O output é feito em KiB, mas eu quero bytes
	mem = int(p.stderr.decode()[:-1]) * 1024
	return mem


N = 100000
dg = 1

# Executable base memory
mem0 = getMem(0, 0)

for g in range(0,100,dg):
	mem = getMem(N,g)
	memPerStruct = (mem-mem0)/N
	print("%d\t%g"%(g, memPerStruct))
