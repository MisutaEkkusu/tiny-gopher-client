#!/usr/bin/python3

import subprocess as sp

dN = 1000000

for i in range(10):
	N = i*dN
	p = sp.run(["/usr/bin/time", "-f", "%M", "./one", str(N)],
	           stdout=sp.PIPE, stderr = sp.PIPE)
	# O output é feito em KiB, mas eu quero bytes
	mem = int(p.stderr.decode()[:-1]) * 1024
	print(N, mem, sep="\t")
