// Testing simple structures with associated memory allocations

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char* argv[])
{	
	//printf("%lu\n", sizeof(s));

	if (argc != 3) {
		exit(2);
	}

	int N = atoi(argv[1]);
	int g = atoi(argv[2]);

	//uint8_t** array = malloc(sizeof(uint8_t*)*N);
	uint16_t** array = malloc(sizeof(uint16_t*)*N);

	int i,j;
	for (i=0; i<N; i++) {
		//array[i] = malloc(g*sizeof(uint8_t));
		array[i] = malloc(g*sizeof(uint16_t));
		for (j=0; j<g; j++) {
			array[i][j] = 255-(i%256);
		}
	}

	// TESTE
	//for (j=0; j<256; j++) {
		//printf("%02X ", *((uint8_t*)array[0] + j));
	//	printf("%02X ", array[0][j] );
	//}
	printf("\n");

	return 0;
}
