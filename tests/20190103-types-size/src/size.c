#include <stdio.h>
//#include <stdio.h>
#include "../../src/tinytypes.h"

int main(int argc, char* argv[])
{	
	size_t lineSize = sizeof(GOPH_line_t);
	printf("%lu\n", lineSize);

	GOPH_line_t l0 = GOPH_line_new();
	GOPH_line_setParams(&l0, '1', "12345678901234567890", "12345678901234567890", "12345678901230123456789abcdef0", 70);
	
	printf("%lu\n", GOPH_line_size(&l0));

	//int N = 10000;
	int N = atoi(argv[1]);

	GOPH_line_t* array = malloc(sizeof(GOPH_line_t)*N);
	int i;
	for (i=0; i<N; i++)
	{
		array[i] = GOPH_line_clone(&l0);
	}

	return 0;
}
