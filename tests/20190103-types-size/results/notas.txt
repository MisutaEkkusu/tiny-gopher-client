2018-12-31

Estou a medir a ocupação em memória com:
$ /usr/bin/time -f "%M" $comand

Primeiro fiz uns teste com uma estrutura com um só char. Descobri que a memória obtida pelo /usr/time é dado em kibibytes e não em kilobytes.

Nos testes com o line_t, o tamanho por estrutura line_t (em bytes) parece ser um múltiplo de 8.
Para confirmar esta suspeita, fui retirando char a char de uma das strings do line_t a ser clonado.
Quando sizeof(GOPH_line_t) + line->len ultrapassa o degrau de um múltiplo de 8, a memória acrescenta N*8 bytes.

Será que as strings são alocadas em blocos de 8 bytes?
