#include<stdio.h>
#include<string.h>

// CONFIGURACOES DO TERMINAL
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>


int main()
{
	// ALTERNATE SCREEN BUFFER - ENTER
	// Output de $ tput smcup | hd
	printf("\e[?1049h\e[22;0;0t");

	// CONFIGURAÇÃO DO TERMINAL
	struct termios old_tio, new_tio;
	// get the terminal settings for stdin
	tcgetattr(STDIN_FILENO,&old_tio);
	// we want to keep the old setting to restore them a the end
	new_tio=old_tio;
	// disable canonical mode (buffered i/o) and local echo
	new_tio.c_lflag &=(~ICANON & ~ECHO);
	// set the new settings immediately
	tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);


	unsigned char c;
	do {
		c = getchar();
		printf("%c\t%d\t%x\n", c, c, c);
	} while (c != 10);


	// restore the former settings
	tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
	
	// ALTERNATE SCREEN BUFFER - EXIT
	// Output de $ tput rmcup | hd
	printf("\e[?1049l\e[23;0;0t");


	return 0;
}
