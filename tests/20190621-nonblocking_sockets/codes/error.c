#include<stdio.h>
#include <errno.h>
int main()
{
	printf("%3d EACCES\n", EACCES);
	printf("%3d EADDRINUSE\n", EADDRINUSE);
	printf("%3d EADDRNOTAVAIL\n", EADDRNOTAVAIL);
	printf("%3d EAFNOSUPPORT\n", EAFNOSUPPORT);
	printf("%3d EAGAIN\n", EAGAIN);
	printf("%3d EALREADY\n", EALREADY);
	printf("%3d EBADF\n", EBADF);
	printf("%3d ECONNREFUSED\n", ECONNREFUSED);
	printf("%3d EFAULT\n", EFAULT);
	printf("%3d EINPROGRESS\n", EINPROGRESS);
	printf("%3d EINTR\n", EINTR);
	printf("%3d EISCONN\n", EISCONN);
	printf("%3d ENETUNREACH\n", ENETUNREACH);
	printf("%3d ENOTSOCK\n", ENOTSOCK);
	printf("%3d EPERM\n", EPERM);
	printf("%3d EPROTOTYPE\n", EPROTOTYPE);
	printf("%3d ETIMEDOUT\n", ETIMEDOUT);
	return 0;
}
