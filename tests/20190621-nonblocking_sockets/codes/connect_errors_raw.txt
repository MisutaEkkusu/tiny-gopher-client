       EACCES For UNIX domain sockets, which are identified by pathname:
              Write permission is denied on the socket file, or search
              permission is denied for one of the directories in the path
              prefix.  (See also path_resolution(7).)

       EACCES, EPERM
              The user tried to connect to a broadcast address without
              having the socket broadcast flag enabled or the connection
              request failed because of a local firewall rule.

       EADDRINUSE
              Local address is already in use.

       EADDRNOTAVAIL
              (Internet domain sockets) The socket referred to by sockfd had
              not previously been bound to an address and, upon attempting
              to bind it to an ephemeral port, it was determined that all
              port numbers in the ephemeral port range are currently in use.
              See the discussion of /proc/sys/net/ipv4/ip_local_port_range
              in ip(7).

       EAFNOSUPPORT
              The passed address didn't have the correct address family in
              its sa_family field.

       EAGAIN For nonblocking UNIX domain sockets, the socket is
              nonblocking, and the connection cannot be completed
              immediately.  For other socket families, there are
              insufficient entries in the routing cache.

       EALREADY
              The socket is nonblocking and a previous connection attempt
              has not yet been completed.

       EBADF  sockfd is not a valid open file descriptor.

       ECONNREFUSED
              A connect() on a stream socket found no one listening on the
              remote address.

       EFAULT The socket structure address is outside the user's address
              space.

       EINPROGRESS
              The socket is nonblocking and the connection cannot be
              completed immediately.  (UNIX domain sockets failed with
              EAGAIN instead.)  It is possible to select(2) or poll(2) for
              completion by selecting the socket for writing.  After
              select(2) indicates writability, use getsockopt(2) to read the
              SO_ERROR option at level SOL_SOCKET to determine whether
              connect() completed successfully (SO_ERROR is zero) or
              unsuccessfully (SO_ERROR is one of the usual error codes
              listed here, explaining the reason for the failure).

       EINTR  The system call was interrupted by a signal that was caught;
              see signal(7).

       EISCONN
              The socket is already connected.

       ENETUNREACH
              Network is unreachable.

       ENOTSOCK
              The file descriptor sockfd does not refer to a socket.

       EPROTOTYPE
              The socket type does not support the requested communications
              protocol.  This error can occur, for example, on an attempt to
              connect a UNIX domain datagram socket to a stream socket.

       ETIMEDOUT
              Timeout while attempting connection.  The server may be too
              busy to accept new connections.  Note that for IP sockets the
              timeout may be very long when syncookies are enabled on the
              server.
