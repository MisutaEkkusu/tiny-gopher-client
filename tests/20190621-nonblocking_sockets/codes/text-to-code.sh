#!/bin/bash

# Texto sacado de
# http://man7.org/linux/man-pages/man2/connect.2.html

echo -e "#include<stdio.h>
#include <errno.h>
int main()
{" > error.c

< connect_errors_raw.txt grep -o \ E[A-Z]* |
cut -c 2- |
sort -u |
awk '{print "\tprintf(\"%3d "$0"\\n\", "$0");"}' >> error.c

echo -e "\treturn 0;
}" >> error.c

gcc error.c -o error
