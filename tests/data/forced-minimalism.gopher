                        FORCED MINIMALISM

We're  getting our _stuff_ back next week, and I have mixed feel-
ings about it.

I've been away from 95% of my  belongings  for  six  months.   We
moved  our  four-person  family  cross country at the very end of
2018 and all of our things went into a pair  of  PODS  containers
and  were  transported 1,800 miles (2,977 km) to be stored fairly
nearby until we sold our old house and bought  a  new  one  here.
We've been living in an apartment in the meantime.

The things I've been glad to have:

  * Clothes
  * Toiletries
  * Bowls, plates, forks, spoons, etc.
  * A little electric kettle to boil water for tea
  * A microwave
  * Basic hand tools and cordless drill
  * Pocket knife
  * Laptop, trackball, mechanical keyboard
  * Kindle Paperwhite
  * A couple physical books
  * Sketchbook, fountain pen, little watercolor kit
  * Notebooks, pencils, pens
  * Box of Important Papers
  * A twin-size Casper memory foam mattress for a bed

The things I've missed not having:

  * Some of my books
  * The rest of my tools
  * Art supplies
  * My desktop computer (especially the screen real estate!)
  * My desk and Steelcase Leap office chair
  * A "proper" bed

That's about it.

If  I  could  only  have _one_ of the things I missed, I think it
would be my office chair!

Otherwise, it's not _stuff_ I miss, but having space  to  stretch
out,  a  room to call my own, and a back yard for the kids to run
and yell in.  Oh, and a garage.  I miss being able to park  in  a
garage  and  carry groceries straight into the house without get-
ting rained on.

Anyway, we're getting our stuff back and I...kinda don't want it.

(Well, mostly I just don't want to have to _deal with it_.)

But, really, living apart from all of that stuff really has given
me  quite a bit of distance from it.  If we open the doors on the
PODS next week and see that it was all destroyed, it wouldn't  be
nearly as heartbreaking as it would have been six months ago.



Spending habits
=================================================================

As our income declined over the last  couple  years  (before  the
move and the new job), we found ourselves becoming what they call
"house poor".  We had physical assets, but we  were  nevertheless
slowly nibbling away at savings to meet our living expenses.

We tightened the belt.  Then we tightened it again.

On  top  of  that, I found that now that I had kids, there wasn't
much point in spending money on movies and games I  wasn't  going
to watch or play anyway.  So bit by bit, my desire to acquire and
collect things slowly died off.

I mean, it took _years_ to turn that  boat  around,  but  it  did
turn.



Keeping the habits?
=================================================================

We're not "poor" anymore.  *And* we're soon going to have room to
store stuff in the new house.

But I think - I hope - we were forced into minimalism long enough
to let the good habits settle in.
