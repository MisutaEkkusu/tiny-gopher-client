#!/usr/bin/python3

import random

CACHE_MAX = 200
CACHE_BLOCK = 8

# numero de items, espaço, indice 1º item, indice ultimo item, lista indice do proximo, lista dos tamanhos, lista dos items

# Vou eliminar blocos inteiros quando tiver de abrir espaço na cache

cache = {
'len'      :0,
'cap'      :0,
'size'     :0,
'first'    :-1,
'last'     :-1,
'next'     :[],
'block'    :[],
'blockLen' :[],
'blockSize':[],
}

def cache_addCapacity(cache):

	cache['cap'] += 1
	cache['next'].append(-1)
	cache['block'].append([-1]*CACHE_BLOCK)
	cache['blockLen'].append(0)
	cache['blockSize'].append(0)
	
	#cache['len'] += 1

	#if cache['len'] == 1:
	#	cache['first'] = newBlockId

	#lastID  = cache['last']
	#if lastID != -1:
	#	cache['next'][lastID] = newBlockId
	#cache['last'] = newBlockId


def cache_removeTopEmpty(cache):
	# Caso tenha chegado aqui, é necessário remover os blocos finais.
	i = cache['cap'] - 1
	while i>-1:
		if cache['blockLen'][i] != 0:
			return
		cache['next'     ].pop()
		cache['block'    ].pop()
		cache['blockLen' ].pop()
		cache['blockSize'].pop()
		cache['cap'] -= 1
		i -= 1


def cache_removeFirstBlock(cache):
	if cache['len'] == 0:
		return

	firstId = cache['first']
	
	# Compor os escalares da cache
	cache['len'  ] -= 1
	cache['size' ] -= cache['blockSize'][firstId]

	# FIXME: Caso o first seja o last?
	# O QUE É O FIRST E O LAST? PODEM SER O MESMO?
	# SERÁ QUE DEVO TER UM BLOCO 0 VIRTUAL (first) QUE NUNCA VAI SER ELIMINADO?
	cache['first']  = cache['next'     ][firstId]

	if cache['len'] == 0:
		cache['last'] = -1
	
	cache['next'     ][firstId] = -1
	cache['blockLen' ][firstId] =  0
	cache['blockSize'][firstId] =  0



def cache_addPage(cache, n):
	if n > CACHE_MAX:
		return
	
	# Esta parte da politica pode ser metida noutro sitio

	# Agora é preciso ver quantos blocos têm de ir abaixo para acomodar esta
	# página
	freeSpace = CACHE_MAX - cache['size']
	i = 0
	while freeSpace < n:
		freeSpace += cache['blockSize'][cache['first']]
		cache_removeFirstBlock(cache)
	

	# Mecanismo

	# IDEIA CENTRAL: Quero adicionar a página ao last block
	# Problemas: O 'last block existe' ?
	# Se existe, tem espaço?
	# Se não tiver espaço, há algum bloco vazio para começar a encher?
	
	lastBlock = -1


	# Existem três acções possíveis:
	# Adicionar a página ao last block
	# (o last block existe e ainda tem espaço)

	# (o last block não existe ou se existe, está cheio)
	# Adicionar a página num block vazio pré-existente

	# (o last block não existe ou se existe, está cheio e não há blocos vazios)
	# Adicionar a página a um block vazio novo
	# No fim, só quero o índice do bloco ao qual vou adicionar a página

	# Há aqui 3 perguntas a responder:
	# 1) Existe last block?
	# 2) O last block está cheio?
	# 3) Existem blocks livres? (len < cap)

	# 1) Obter o índice do bloco a encher
	# Vou testar se o last block consegue memorizar a página.
	if cache['last'] == -1 or cache['blockLen'][cache['last']] == CACHE_BLOCK:

		if cache['len'] == cache['cap']:
			cache_addCapacity(cache)
			lastBlock = cache['len']
		else:
			# Find empty block
			for i in range(cache['cap']):
				if cache['blockLen'][i] == 0:
					lastBlock = i
					break

		cache['len' ] += 1
		if cache['len'] > 1:
			cache['next'][cache['last']] = lastBlock
		cache['last']  = lastBlock

	else:
		lastBlock = cache['last']
	
	# 2) Agora tenho o índice do próximo bloco a encher
	lastBlockLen = cache['blockLen'][lastBlock]
	cache['block'    ][lastBlock][lastBlockLen] = n
	cache['blockLen' ][lastBlock] += 1
	cache['blockSize'][lastBlock] += n

	cache['size'] += n
	if cache['first'] == -1:
		cache['first'] = lastBlock


def positiveIntGauss(m, s):
	r = -1
	while r<0:
		r = int(random.gauss(m,s))
	return r
	

def cache_print(cache):
	print("len:", cache['len'], "  cap:", cache['cap'], "  size:", cache['size'])
	print("first:", cache['first'], "  last:", cache['last'])
	print("next     :", cache['next'])
	print("blockLen :", cache['blockLen'])
	print("blockSize:", cache['blockSize'])
	print("block:")
	for i,b in enumerate(cache['block']):
		print(i, ":", b)

def cache_verify_printError(msg):
	print("\033[1;31mVERIFY: " + msg + "\033[0m")


def cache_verify(cache):

	# Verify Capacity
	# This is the first and most important test
	fCapCorrect = True
	for elem in ['next', 'blockLen', 'blockSize']:
		if len(cache[elem]) != cache['cap']:
			cache_verify_printError(elem + " LEN NOT EQUAL TO CAP")
			fCapCorrect = False

	# Verify first and last indexes
	if cache['first'] < -2 or cache['first'] >= cache['cap']:
			cache_verify_printError(" first HAS BAD INDEX")
	if cache['last'] < -2 or cache['last'] >= cache['cap']:
			cache_verify_printError(" last HAS BAD INDEX")
	
	# Verify all next
	for (i,n) in enumerate(cache['next']):
		if n < -2 or n >= cache['cap']:
			cache_verify_printError("next[%d] HAS BAD INDEX"%i)
	
	# Verify sequence
	next_isVisited = [0]*cache['cap']
	n = 0
	i  = cache['first']
	i0 = i
	while True:
		if i == -1:
			break
		if next_isVisited[i]:
			cache_verify_printError("next=%d ALREADY VISITED"%i)
		else:
			next_isVisited[i] = 1
		n += 1
		i0 = i
		i  = cache['next'][i]
	
	# Verify list len
	if n != cache['len']:
		cache_verify_printError("WRONG LEN")
	
	# The links must end on last
	if i0 != cache['last']:
		cache_verify_printError("THE LIST DOES NOT END IN last")

	# Verify if next of last is -1
	if cache['len'] > 0 and cache['next'][cache['last']] != -1:
		cache_verify_printError("last HAS next NOT -1")
	

	# Verify blockSize sum
	if sum(cache['blockSize']) != cache['size']:
		cache_verify_printError("size NOT CORRECT")
	
	# Verify blockSize per block
	for i in range(cache['cap']):
		blockLen = cache['blockLen'][i]
		s = sum(cache['block'][i][0:blockLen])
		if s != cache['blockSize'][i]:
			cache_verify_printError("BLOCK %d HAS A BAD blockSize"%i)

#def cache_add(cache, n):
	
# LINKED LIST EM BLOCOS

# ACTIONS FIXED
#action = [3,3]

# ACTIONS RANDOM
#random.seed(5)
#action = [ random.randint(0,3) for i in range(10)]

# ACTIONS BIASED RANDOM
#actionChoice = [0,1,2,3,3,3,3,3,3,3,3]
actionChoice = [3]
action = random.choices(actionChoice, k=200)

print(-1)
cache_print(cache)
cache_verify(cache)
print()

for i in range(len(action)):
	a = action[i]
	
	print("[[", i, "]]", a, end = " ")

	if a == 0:
		print("addCapacity")
		cache_addCapacity(cache)
	if a == 1:
		print("removeFirstBlock")
		cache_removeFirstBlock(cache)
	if a == 2:
		print("removeTopEmpty")
		cache_removeTopEmpty(cache)
	if a == 3:
		print("addPage")
		#cache_addPage(cache, random.randint(0,20))
		cache_addPage(cache, positiveIntGauss(10, 5))
	if a == 4:
		print("addPage and removeTopEmpty")
		cache_addPage(cache, random.randint(0,20))
		cache_removeTopEmpty(cache)
	
	cache_print(cache)
	cache_verify(cache)
	print()


if 0:
	for i in range(33):
		cache_addPage(cache, i)
		cache_removeTopEmpty(cache)
		
		cache_print(cache)
		cache_verify(cache)
		print()
