#!/bin/bash

dataDir="results"

n=$1

dataFn="$dataDir/t$n-data.txt"

./cache-block.py | awk '/size/{print $2"\t"$4"\t"$6}' > $dataFn

gnuplot -e "n=$n" run/plot.sh 
