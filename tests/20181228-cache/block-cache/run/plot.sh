#!/usr/bin/gnuplot

fn = "results/t".n."-data.txt"

set terminal png
set key bottom

set output "results/t".n."-plot-lencap.png"
plot fn u 1 title "len" w l, fn u 2 title "cap" w l


set output "results/t".n."-plot-size.png"
plot fn u 3 title "size" w l

#set output "results/t0-plot-all.png"
#plot\
#	fn u ($1*8*10) title "len" w l,\
#	fn u ($2*8*10) title "cap" w l,\
#	fn u 3 title "size" w l
