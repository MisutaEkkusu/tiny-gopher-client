// Testing conversion from string to type_t and vice-versa

#include <stdlib.h>
#include <stdio.h>
#include "tinytypes.h"
#include "tinycache.h"
#include "tinylogic.h"
#include "tinyfetch.h"
#include "tinydebug.h"

GOPH_page_t getPageFromLine(GOPH_line_t* line)
{
	GOPH_usr_t   user  = GOPH_usr_new();
	GOPH_page_t  page  = GOPH_page_new(line->type);

	GOPHER_logic_setUserSelect(&user, *line);
	GOPH_fetch(&user, &page);
	GOPH_Close(user.sockid);

	GOPH_usr_reset(&user);

	return page;
}

void test0()
{
	// PAGE 0
	GOPH_line_t line0 = GOPH_line_new();
	GOPH_line_setParams(&line0, '1', "Initial page", "\n",
	                    "gopher.floodgap.com", 70);
	GOPH_page_t page0 = getPageFromLine(&line0);
	

	// PAGE 1
	char test1[] = "0Using your web browser to explore Gopherspace	/gopher/wbgopher	gopher.floodgap.com	70\r\n";
	GOPH_line_t line1;
	GOPH_line_parser(test1, strlen(test1), &line1);
	GOPH_page_t page1 = getPageFromLine(&line1);
	

	// CACHE
	GOPH_cache_t cache = GOPH_cache_new();
	
	// Duplicate page
	GOPH_page_t page2 = GOPH_page_clone(&page0);
	GOPH_page_t page3 = GOPH_page_clone(&page0);

	//GOPH_page_print(&page);

	printf("after | indexPage: %d\n", GOPH_cache_indexPage(&cache, &line0));
	printf("after | indexPage: %d\n", GOPH_cache_indexPage(&cache, &line1));
	printf("cache->len: %lu\n", cache.len);
	printf("cache->cap: %lu\n", cache.cap);

	GOPH_cache_savePage(&cache, &line0, &page0);
	GOPH_cache_savePage(&cache, &line0, &page2);
	GOPH_cache_savePage(&cache, &line0, &page3);
	GOPH_cache_savePage(&cache, &line1, &page1);

	printf("after | indexPage: %d\n", GOPH_cache_indexPage(&cache, &line0));
	printf("after | indexPage: %d\n", GOPH_cache_indexPage(&cache, &line1));
	printf("cache->len: %lu\n", cache.len);
	printf("cache->cap: %lu\n", cache.cap);

	
	// FIXME: Estudar a questão da ordem dos frees.
	// Se se fizer free à page a seguir à cache, dá segfault.
	GOPH_cache_reset(&cache);
	GOPH_line_reset(&line0);
	GOPH_line_reset(&line1);

	//GOPH_page_reset(&page1);

	
	/*
	char* buffer;
	int n = 0;
	n = GOPH_line_toString(&line, &buffer);

	int i;
	for (i=0; i<line.len; i++) {
		fprintf(stderr, "%02x: %02x %c\n", i, line.ptr[i], line.ptr[i]);
	}

	fprintf(stderr, "n:%d %x\n", n, n);

	printf("%s", buffer);
	*/
}


int main()
{ 
	test0();

	return 0;
}
