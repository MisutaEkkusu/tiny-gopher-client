// Test of line cloning

#include "../../src/tinyparser.h"
#include "../../src/tinyutils.h"

int main()
{
	GOPH_line_t line;
	line.type = '1';
	line.disp = GOPH_str_new("Iron Maiden");
	line.sel  = GOPH_str_new("iron");
	line.dom  = GOPH_str_new("www.iron.com");
	line.port = 666;
	line.linkIndx = 44;

	GOPH_line_print(&line);

	GOPH_line_t clone = GOPH_line_clone(&line);

	line.disp[0] = 'K';
	
	printf("\n[original]\n");
	GOPH_line_print(&line);

	printf("\n[clone]\n");
	GOPH_line_print(&clone);

	GOPH_line_kill(&line);
	GOPH_line_kill(&clone);

	return 0;
}
