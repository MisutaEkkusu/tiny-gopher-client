// Testing conversion from string to type_t and vice-versa

#include "../../src/tinytypes.h"

void test0()
{
	GOPH_line_t line = GOPH_line_new();
	GOPH_line_setParams(&line, '1', "Initial page", "\n",
	                    "gopher.floodgap.com", 70);
	char* buffer;
	int n = 0;
	n = GOPH_line_toString(&line, &buffer);

	int i;
	for (i=0; i<line.len; i++) {
		fprintf(stderr, "%02x: %02x %c\n", i, line.ptr[i], line.ptr[i]);
	}

	fprintf(stderr, "n:%d %x\n", n, n);

	printf("%s", buffer);
}


void test1()
{	
	char test[] = "0Using your web browser to explore Gopherspace	/gopher/wbgopher	gopher.floodgap.com	70\r\n";
	
	// CREATE line_t from string
	GOPH_line_t lineTest;
	GOPH_line_parser(test, strlen(test), &lineTest);

	// RECOVER string from line_t from string
	char* buffer;
	int n = 0;
	n = GOPH_line_toString(&lineTest, &buffer);

	// COMPARE old and new string
	int c = strcmp(test, buffer);

	fprintf(stderr, "c: %d\n", c);
	if (c==0) {
		fprintf(stderr, "The strings are equal.\n");
	}

	printf("%s", test);
	printf("%s", buffer);

}

int main()
{ 
	test1();

	return 0;
}
