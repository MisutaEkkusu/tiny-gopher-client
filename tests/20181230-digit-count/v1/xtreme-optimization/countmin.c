#include <stdint.h>
#include <stdio.h>

// Optimização extrema do algoritmo base.
size_t GOPH_str_i32_len(int32_t num)
{
	const int32_t len[4] = {10, 100, 10000, 100000000};
	const int32_t dig[4] = {1, 2 , 4, 8};
	int32_t digits = 0;
	int32_t i = 4;
	while(i--) {
		if (num >= len[i]) {
			// Os números a somar são números de base 2 distintos, logo a soma
			// pode ser feita com um or.
			digits |= dig[i];
			num /= len[i];
		}
	}
	// Esta soma tem que ser feita após o loop, para poder usufruir da soma com
	// or.
	digits += 1 + (num < 0);
	return digits;
}

int main()
{
	int n = GOPH_str_i32_len(345);
	printf("%d\n", n);
	return 0;
}
