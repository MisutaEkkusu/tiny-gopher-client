	.file	"countmin.c"
	.text
	.globl	GOPH_str_i32_len
	.type	GOPH_str_i32_len, @function
GOPH_str_i32_len:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movl	%edi, -52(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$10, -32(%rbp)
	movl	$100, -28(%rbp)
	movl	$10000, -24(%rbp)
	movl	$100000000, -20(%rbp)
	movl	$1, -44(%rbp)
	cmpl	$0, -52(%rbp)
	jns	.L2
	addl	$1, -44(%rbp)
.L2:
	movl	$0, -36(%rbp)
	movl	$3, -40(%rbp)
	jmp	.L3
.L5:
	movl	-40(%rbp), %eax
	cltq
	movl	-32(%rbp,%rax,4), %esi
	movl	-52(%rbp), %eax
	cltd
	idivl	%esi
	movl	%eax, -36(%rbp)
	cmpl	$0, -36(%rbp)
	je	.L4
	movl	-40(%rbp), %eax
	movl	$1, %edx
	movl	%eax, %ecx
	sall	%cl, %edx
	movl	%edx, %eax
	addl	%eax, -44(%rbp)
	movl	-36(%rbp), %eax
	movl	%eax, -52(%rbp)
.L4:
	subl	$1, -40(%rbp)
.L3:
	cmpl	$0, -40(%rbp)
	jns	.L5
	movl	-44(%rbp), %eax
	cltq
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	je	.L7
	call	__stack_chk_fail@PLT
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	GOPH_str_i32_len, .-GOPH_str_i32_len
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$345, %edi
	call	GOPH_str_i32_len
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
