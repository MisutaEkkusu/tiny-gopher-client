#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>

// Optimização extrema do algoritmo base.
size_t GOPH_str_i32_len(int32_t num)
{
	const int32_t len[4] = {10, 100, 10000, 100000000};
	const int32_t dig[4] = {1, 2 , 4, 8};
	int32_t digits = 0;
	int32_t i = 4;
	while(i--) {
		if (num >= len[i]) {
			// Os números a somar são números de base 2 distintos, logo a soma
			// pode ser feita com um or.
			digits |= dig[i];
			num /= len[i];
		}
	}
	// Esta soma tem que ser feita após o loop, para poder usufruir da soma com
	// or.
	digits += 1 + (num < 0);
	return digits;
}

size_t dig_math(int32_t num)
{
	if (num == 0)
		return 1;
	return (int32_t)log10(num) + 1;
}

size_t dig_math_s(int32_t num)
{
	if (num == 0)
		return 1;
	return (int32_t)log10(abs(num)) + 1 + (num<0);
}

size_t dig_string(int32_t num)
{
	char buf[16];
	snprintf(buf, 16, "%d", num);
	return strlen(buf);
}


double dtime()
{
	const double nano = 1e-9;
	struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	return (double)tp.tv_sec + (double)tp.tv_nsec * nano;
}

int64_t i64time()
{
	const int64_t GIGA = 1000000000;
	struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	return tp.tv_sec*GIGA + tp.tv_nsec;
}

void test_dtime()
{
	double t[20];

	for(int i=0; i<20; i++) {
		t[i] = dtime();
	}
	for(int i=0; i<19; i++) {
		printf("%.16g\n",t[i+1] - t[i]);
	}
}

void test_i64time()
{
	int64_t t[20];

	for(int i=0; i<20; i++) {
		t[i] = i64time();
	}
	for(int i=0; i<19; i++) {
		printf("%ld\n",t[i+1] - t[i]);
	}
}


int main()
{
	srand(time(NULL));

	//int N = 10000000;
	int N = 1;

	int32_t n1, n2, n3, r;
	for (int j = 0; j<8; j++) {
		N *= 10;
		double tic = dtime();

		for (int i = 0; i<N; i++) {
			r = random();
			//n1 = GOPH_str_i32_len(r);
			//n1 = dig_string(r);
			n1 = dig_math_s(r);
			/*
			n2 = dig_math(r);
			if (n1 != n2) {
				printf("FUCK!\n");
				printf("%d %d %d\n", r, n1, n2);
				exit(1);
			}
			*/
		}

		double toc = dtime();
		printf("%d\t%g\n", N, toc-tic);
	}

	return 0;
}
