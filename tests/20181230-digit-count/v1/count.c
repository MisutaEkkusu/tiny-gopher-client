#include <stdint.h>
#include <stdio.h>

// RACIONAL
// A ideia é baseada numa mistura de bisecção com divisão sistemática por 10.
// Ao dividir um número por uma potência de 10, 10^i, e verificar se o resultado
// é diferente de 0, estou a testar se o número tem mais de i dígitos. Se não,
// passa-se para testes menores.
// Este teste poderia ser feito potência a potência, reduzindo (ou aumentando) o
// 'i' uma unidade de cada vez.
// Vamos supor que se determina que um número terá menos de 16 dígitos. Se no
// passo seguinte testar com i=8, então vou eliminar metade das hipóteses entre
// 1 e 16.
//
// Matematicamente, um numero n pode ser escrito como:
// n = m * 10 ^ sum(i=0:N, b_i * 2^i), 1 <= m < 10
// As divisões por 10^i funcionam como subtracções de 'i' ao expoente do número.
// Se i for um número de base 2, 2^k, então as divisões funcionam como bitshift
// de k para a direita para a decomposição binária do expoente (b_N, ..., b_0).
//
// Quando se quer determinar a composição binária de um número, costuma-se usar
// o comando (n>>k) & 1 para os vários dígitos k.
// O comando equivalente, para descobrir o bit b_k será (n//10^(2^k))!=0
// Uma abordagem alternativa, pode verificar se (n >= 10^(2^k)). Se isto for
// verdade, então terá mais de k dígitos (PENSAR MELHOR, ISTO NÃO ESTÁ CLARO)
//
//(ATENÇÃO : não estou a fazer decomposição binária, estou a fazer bisecção)
//
// Ora, o que andamos à procura é de ... (TENHO DE PENSAR MELHOR NO ASSUNTO)
// ...

size_t GOPH_str_i32_len(int32_t num)
{
	const int32_t len[4] = {10, 100, 10000, 100000000};
	const int32_t dig[4] = {1, 2 , 4, 8};
	int32_t digits = 1 + (num < 0);
	for (int32_t i=3; i>=0; i--) {
		if (num >= len[i]) {
			digits += dig[i];
			num /= len[i];
		}
	}
	return digits;
}


void test0()
{
	int32_t n = 9;
	//shift(n);
	//ten(n);
	int32_t d0, d1;
	int32_t n0 = 1;
	int32_t n1 = 0;
	for(int i=0; i<12; i++) {
		n0 *= 10;
		n1 = -n0;
		for(int j=-1; j<2; j++) {
			int d0 = GOPH_str_i32_len(n0+j);
			int d1 = GOPH_str_i32_len(n1-j);
			printf("%d\t%d\n", n0+j, d0);
			printf("%d\t%d\n", n1-j, d1);
		}
	}
}


int main()
{
	test0();
	return 0;
}
