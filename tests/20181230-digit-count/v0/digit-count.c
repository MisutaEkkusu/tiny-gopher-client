// Test digit count

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>

int32_t test0(int32_t x)
{
	if (x==0) {
		return 1;
	} else {
		return ceil(log10(x + 1));
	}
}

int32_t test1(int32_t x)
{
	int32_t n = 0;
	int32_t z = x;
	do {
		n++;
		z /= 10;
	}while(z);
	return n;
}

int32_t test2(int32_t x)
{
	int32_t n = 0;
	int32_t z = 1;
	do {
		n++;
		z *= 10;
	}while(z<=x);
	return n;
}

int main(int argc, char* argv[])
{
	if (argc != 2)
		exit(2);

	int t = atoi(argv[1]);

	if (t<0 || t>2)
		exit(2);
	

	int32_t (*test[3])(int32_t) = {test0, test1, test2};

	int32_t i, j, x;
	int32_t n;
	
	//n * test0;

	for(j=0; j<1; j++) {
		for(i=0; i<200; i++) {
			x = i*i*i;
			n = (*test[t])(x);
			printf("%d %d\n", x, n);
		}
	}
	return 0;
}
