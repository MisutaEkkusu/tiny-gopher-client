#include <stdio.h>
#include "../../src_lnx/tinytypes.h"

int parseUrl(char* url)
{
	GOPH_line_t line = GOPH_line_new();
	GOPH_line_urlToLine(&line, url);
	GOPH_line_print(&line);
	GOPH_line_free(&line);
	return 0;
}


/*
 EXEMPLOS IGUAIS
---
[diamantino@mbp ~]$ lynx gopher://sdf.org
[diamantino@mbp ~]$ lynx gopher://sdf.org/
[diamantino@mbp ~]$ lynx gopher://sdf.org/1
[diamantino@mbp ~]$ lynx gopher://sdf.org/1/
---
DISPLAY ERRADO
[diamantino@mbp ~]$ lynx gopher://sdf.org/0
----
CORRECTO
[diamantino@mbp ~]$ lynx gopher://sdf.org/1/aged-maps
[diamantino@mbp ~]$ lynx gopher://sdf.org/1/aged-maps/
*/

// Testing semantically the same address
void test1()
{
	parseUrl("gopher://sdf.org");
	printf("\n");
	parseUrl("gopher://sdf.org/");
	printf("\n");
	parseUrl("gopher://sdf.org/1");
	printf("\n");
	parseUrl("gopher://sdf.org/1/");
	printf("\n");
	parseUrl("  gopher://sdf.org/1/");
	printf("\n");
	parseUrl("gopher://sdf.org/1/   ");
	printf("\n");
	parseUrl("    gopher://sdf.org/1/   ");
	printf("\n");
}


// Testing more separators than needed
void test2()
{
	parseUrl("gopher://sdf.org/1/aged-maps");
	printf("\n");
	parseUrl("gopher://sdf.org/1/aged-maps/");
	printf("\n");
	parseUrl("gopher://sdf.org/1/aged-maps/benfica");
	printf("\n");
}


// Testing bad prefix
void test3()
{
	parseUrl("goxher://sdf.org/1/aged-maps/");
	printf("\n");
}


// Testing endlines
void test4()
{
	parseUrl("gopher://sdf.org/1/aged-maps/\r\n");
	printf("\n");
	parseUrl("gopher://sdf.org/1/aged-maps\r\n");
	printf("\n");
	parseUrl("gopher://sdf.org/1/aged-maps/\n");
	printf("\n");
	parseUrl("\t  \t gopher://sdf.org/1/aged-maps/\t  \n");
	printf("\n");
}
	


int main(int argc, char* argv[])
{
	if (argc != 2) {
		exit(2);
	}

	switch(atoi(argv[1])) {
		case 1:
			test1();
			break;
		case 2:
			test2();
			break;
		case 3:
			test3();
			break;
		case 4:
			test4();
			break;
	}		

	return 0;
}
