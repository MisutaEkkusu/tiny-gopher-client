#!/usr/bin/python3

def url2line(url):
	line = {"dom":"", "sel":"", "type":-1}

	begin = "gopher://"
	begin_len = len(begin)

	url_len = len(url)
	#It must start by "gopher://"
	if url[0:begin_len] != begin:
		return 1
	
	dom_pos = begin_len

	# Find first '/'
	dom_end = -1
	dom_len = -1

	# Atenção ao loop à C
	i = dom_pos
	while( i < url_len):
		if url[i] == '/':
			dom_end = i
			break
		i+=1
	
	if dom_end == -1:
		dom_len = url_len - dom_pos
	else:
		dom_len = i - dom_pos
	
	line['dom'] = url[dom_pos:dom_pos+dom_len]

	if url_len > i+1:
		line['type'] = url[i+1]
		i+=2
	else:
		line['type'] = '1'
		i+=1
		
	
	print("url_len:", url_len, "  dom_len:", dom_len, "  i:", i)


	return line
	#Find second '/'

def test0():
	urlList = [\
	"gopher://sdf.org",
	"gopher://sdf.org/",
	"gopher://sdf.org/1",
	"gopher://sdf.org/1/"]

	lines = map(url2line, urlList)
	for l in lines:
		print(l)

def test1():
	urlList = [\
	"gopher://sdf.org/1/aged-maps",
	"gopher://sdf.org/1/aged-maps/"]


test0()
