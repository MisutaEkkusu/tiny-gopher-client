#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/ioctl.h>

#include "../../src/tinydisp.h"
#include "../../src/tinyparser.h"
#include "../../src/tinyutils.h"


int main(int argc, char* argv[]) {

	if (argc != 2) {
		printf("usage: ./menu gopher_file\n");
		exit(1);
	}

	char* filename = argv[1];
	
	
	// Leitura do ficheiro
	GOPH_page_t GOPHER_Page = {0};
	GOPH_get_file(filename, &GOPHER_Page);

	
	// Apresentação do conteúdo
	GOPH_page_show(GOPHER_Page);

	return 0;
}
