#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>

#include "../../src/tinytypes.h"
#include "../../src/tinyutils.h"
#include "../../src/tinydisp.h"
#include "../../src/tinydebug.h"


int main(int argc, char* argv[]) {
//int main() {
	
	if (argc != 2) {
		printf("usage: ./menu gopher_file\n");
		exit(1);
	}

	char* filename = argv[1];

	// Leitura do ficheiro
	GOPH_page_t page = GOPH_page_new();
	GOPH_get_file_txt(filename, &page);
	// Force page type
	//page.type = '1';

	//GOPH_render_t render = GOPH_render_new();
	//GOPH_page_render(&page, &render);
	//GOPH_render_dump(&render);
	

	// Apresentação do conteúdo
	int32_t screen_top = 0;
	int32_t link_n = 0;
	int32_t line_n = GOPH_page_show(&page, &screen_top, &link_n);
	
	/*
	printf("%p\n", line_ptr);
	printf("%s\n", (*line_ptr).disp);
	*/

	// Memory management
	GOPH_page_free(&page);

    return 0;

}
