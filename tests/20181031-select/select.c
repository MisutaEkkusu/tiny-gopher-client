#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>

#include "../../src/tinytypes.h"
#include "../../src/tinydebug.h"
#include "../../src/tinyparser.h"
#include "../../src/tinyutils.h"
#include "../../src/tinydisp.h"


int main(int argc, char* argv[]) {
	
	if (argc != 2) {
		printf("usage: ./menu gopher_file\n");
		exit(1);
	}

	char* filename = argv[1];

	// Leitura do ficheiro
	GOPH_page_t GOPHER_Page = GOPH_page_new('1');
	GOPH_get_file(filename, &GOPHER_Page);

	int32_t page_top = 0;
	int32_t link_n = -1;

	// Apresentação do conteúdo
	int32_t num = GOPH_page_show(&GOPHER_Page, &page_top, &link_n);

	// Memory management
	GOPH_page_kill(&GOPHER_Page);

    return 0;

}
