/* Cursor addressing in C - original by Scott Aamodt */
#include <stdio.h>

#define esc 27
#define cls printf("%c[2J",esc)
#define pos(row,col) printf("%c[%d;%dH",esc,row,col)

main()
{
	cls; /* clear the screen */
	pos(9,28); /* position cursor at row 9, column 28 */
	printf("**************"); /* note there is no \n */
	pos(10,28); /* go down to next line */
	printf("* Hi There *"); /* aligns under *’s */
	pos(11,28); /* next line will be bold, blinking */
	printf("* %c[1;5mHi There%c[0m *",esc,esc);
	/* turn on ... turn off */
	pos(12,14); /* next two lines double high */
	/* but also double width - 14 double widths = 28 single */
	printf("%c#3*HELLO*",esc); /* top half */
	pos(13,14); /* must follow up with 2nd identical line */
	printf("%c#4*HELLO*",esc); /* top half */
	pos(14,28); /* close off the box - no need to reset height */
	printf("**************"); /* still no \n */
	pos(20,1); /* "park" cursor - after execution, terminal
	displays OS prompt wherever you left off - don’t want
	to mess up display */
	return 0;
}
