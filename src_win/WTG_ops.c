#include "WTG_ops.h"

/*
	SOCKET SETUP
	Prepares the socket variable for connection

*/

void WTG_MSG(WTG_Error_t errorT){
	
	switch(errorT){
		
		case ERRALLOC:
			fputs("Error allocating memory!\n",stderr);
			break;
			
		case ERRIO:
			fputs("Error IO!\n",stderr);
			break;
			
		case ERRSOCK:
			fputs("Error Socket!\n",stderr);
			break;
		
	}
	
}

int WTG_SetupSocket(conState_t* cs){
	
	cs->iFamily = AF_INET;
	cs->iType = SOCK_STREAM;
	cs->iProtocol = IPPROTO_TCP;
	
	cs->iSocket = socket(cs->iFamily,cs->iType,cs->iProtocol); //create socket
	if(cs->iSocket == INVALID_SOCKET){
		WTG_MSG(ERRSOCK);
		return -2;
	}
	
	return 1;
	
}

/*

	SETUP CONNECTION
	Does DNS lookup to find IP address.
	May include a direct IP mode

*/

int WTG_SetupConnection(conState_t* cs, char* url, uint16_t port){
	
	int i = 0;	
	
	cs->sockAddr.sin_family = AF_INET;
	cs->sockAddr.sin_port = htons(port);
	
	if(url==NULL)
		return 0;
	
	cs->remoteHost = gethostbyname(url);

	if(cs->remoteHost==NULL){
		switch(WSAGetLastError()){
			
			case 0:
				
				break;
				
			case WSAHOST_NOT_FOUND:
				printf("Host was not found.\n");
				break;
			
			case WSANO_DATA:
				printf("No data record found\n");
				break;
				
			default:
				printf("Unknown error\n");
				break;
				
		}
		return -1;
	} 

	if (cs->remoteHost->h_addrtype == AF_INET)
	{
		while (cs->remoteHost->h_addr_list[i] != 0) {
			cs->sockAddr.sin_addr.s_addr = *(u_long *) cs->remoteHost->h_addr_list[i++];
			printf("\tIP Address #%d: %s\n", i, inet_ntoa(cs->sockAddr.sin_addr));
		}
	}
	
	else if (cs->remoteHost->h_addrtype == AF_NETBIOS)
	{   
		return 2;
	} 

	printf("name: %s\n",cs->remoteHost->h_name);
	return 1;
}

/*
	CONNECT SOCKET
	Attempts connection of socket to indicated address
	return 0 on failure

*/

int WTG_Connect(conState_t* cs){
	
    if (connect(cs->iSocket, (SOCKADDR *) &cs->sockAddr, sizeof (cs->sockAddr)) == SOCKET_ERROR) {
        wprintf(L"connect function failed with error: %ld\n", WSAGetLastError());
        if (closesocket(sessState.cState.iSocket) == SOCKET_ERROR)
            wprintf(L"closesocket function failed with error: %ld\n", WSAGetLastError());
        WSACleanup();
        return 0;
    }
	
	return 1;
}
/*
	INITIALIZE SESSION
	Self Explanatory

*/

int WTG_InitSession(struct session* sess){
	
	fState = WSAStartup(MAKEWORD(2, 2), &wsaData); //initializes winsock dll
    if (fState != 0) {
        return -1;
    }
/*
	if(WTG_SetupSocket(&sessState.cState)!=1)
	{
		WSACleanup(); //ends winsock dll
		return -2;
	}
*/	
	return 1;
}




/*
	END SESSION
	Cleans up the system and session structure
*/

int WTG_EndSession(struct session* sess){
	
	//shutdown(sess->cState.iSocket,SD_BOTH); //breaks communication
	closesocket(sess->cState.iSocket); //closes socket
	WSACleanup(); //ends winsock dll

	return 1;
	
}

/*
	RECEIVE FUNCTION:
	Treats all incoming data as array of chars
	Parsing is done a posteriori.

*/

int WTG_Recv(struct session* sess){
	
	uint32_t i = 0;
	uint32_t Msg_buffsize = 0;
	uint32_t Msg_bytesRead = 0;
	uint32_t Msg_bytesRecv = 0;
	char Msg_recv[MSG_BLOC_SIZE];
	
	
	if(sess->dlBuffer != NULL){
		printf("Download buffer not free!\n");
		return -1;
	}

	do{
		Msg_bytesRecv = recv(sess->cState.iSocket,Msg_recv,MSG_BLOC_SIZE,0);
		
		if(Msg_bytesRecv>0){
			
			sess->dlBuffer = realloc(sess->dlBuffer,Msg_bytesRead+Msg_bytesRecv);
			
			if(sess->dlBuffer == NULL)
				continue;
			else
				memcpy(sess->dlBuffer+Msg_bytesRead,Msg_recv,Msg_bytesRecv);
			
			Msg_bytesRead += Msg_bytesRecv;
			
			
		}
		
	}while(Msg_bytesRecv>0);
	
	sess->dlbufsiz = Msg_bytesRead;
	//printf("%s\n",sess->dlBuffer);
	putchar('\n');

	return 1;
}

uint32_t WTG_ParseDirectory(struct session* sess){
	
	uint32_t i = 0, j=0;
	uint32_t offset = 0;
	uint32_t ntabs = 0; // ntabs is tab delimiter count
	uint32_t textlen = 0; //text data length
	
	uint8_t isnewline = 1;
	
	uint16_t lineTexOffset;
	uint16_t lineSelOffset;
	uint16_t lineURLOffset;
	
	sess->pageNumLines = 0;
	/*
	sess->lines = malloc(sizeof(WTG_Line_t)*(sess->pageNumLines));
	
	if(sess->lines == NULL){
		WTG_MSG(ERRALLOC);
		return 0;
	}
	*/
	while(i<sess->dlbufsiz){
		
		if(i<sess->dlbufsiz-2 && strncmp(&sess->dlBuffer[i],"\n.",2)==0)
			break;	

		
		if(isnewline){
			sess->lines = realloc(sess->lines,sizeof(WTG_Line_t)*(sess->pageNumLines+1));
			if(sess->lines == NULL){
				WTG_MSG(ERRALLOC);
				break;
			}
			//sess->lines[sess->pageNumLines].type = sess->dlBuffer[i];
			
			//while(sess->dlBuffer[i]
		}
		
		if(sess->dlBuffer[i] == '\n'){
			sess->pageNumLines++;
			isnewline = 1;
		}
		
		putc(sess->dlBuffer[i],stdout);
		
		i++;
		
	}
	
	printf("Page lines number: %d\n",sess->pageNumLines);
	return 1;
}

void DrawLoop(){
	
	
	
	
	
}

//MAIN LOOP
int WTG_MainLoop(void){
	uint32_t i = 0;
	char msg[] = "\n";

	sessState.exit = 0;
	
	if(WTG_InitSession(&sessState)!=1)
		return -1;
	
	//Begin main loop
	while(!sessState.exit){


		if(WTG_SetupSocket(&sessState.cState)!=1)
		{
			break;
		}
		
		if(WTG_SetupConnection(&sessState.cState, "gopher.floodgap.com", 70)!=1)
			return -1;
		
		if(WTG_Connect(&sessState.cState)!=1)
			return -2;
		
		if (send(sessState.cState.iSocket,msg,(int)strlen(msg),0) == SOCKET_ERROR) {
			wprintf(L"send failed with error: %d\n", WSAGetLastError());
			closesocket(sessState.cState.iSocket);
			WSACleanup();
			return -3;
		}
		
		WTG_Recv(&sessState);
		printf("Bytes received: %d\n",sessState.dlbufsiz);
		//printf("%s\n",sessState.dlBuffer);
		
		WTG_ParseDirectory(&sessState);
		
		DrawLoop();	
		
		
		//do buffer cleanups
		if(sessState.dlbufsiz>0)
			free(sessState.dlBuffer);
		
		if(sessState.pageNumLines>0){
			
			for(i=0;i<sessState.pageNumLines;i++)
				free(sessState.lines[i].text);
			
			free(sessState.lines);
		}
		
		sessState.dlBuffer = NULL;
		sessState.lines = NULL;
		
		sessState.exit = 1;
		printf("Reached end..\n");
		shutdown(sessState.cState.iSocket,SD_BOTH); //breaks communication
	}
	
	WTG_EndSession(&sessState);
	return 1;
}