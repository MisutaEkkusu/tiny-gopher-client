#ifndef _WTGOPS_H_
#define _WTGOPS_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <winsock2.h>

#include "WTG_winops.h"


#define MSG_BLOC_SIZE 128

#define LINSTRSIZ 256 //global line string block size limit (for each field of the line)

int fState; //function state
	/*	return types:
		-2 - socket error: Invalid
		-1 - host error: use WSAGetLastError
		 0 - setup but not connected
		 1 - connection successful
		 2 - netbios connection found
	*/

typedef enum errorType {ERRALLOC, ERRIO, ERRSOCK} WTG_Error_t;
	
WSADATA wsaData;

typedef struct conState{
	
	int iFamily;
	int iType;
	int iProtocol;
	
	SOCKET iSocket;
	struct hostent *remoteHost;
	struct sockaddr_in sockAddr;
	
}conState_t;

typedef struct WTG_Line{
	
	uint8_t type; //line type
	
	char *text;
	
	uint32_t index; //line index
	uint32_t dlbufoffset; //line start offset on buffer
	
	uint16_t port; //line destination port
	char URL[LINSTRSIZ]; //line destination URL
	char selector[LINSTRSIZ]; //line destination selector
	
}WTG_Line_t;

struct session{
	
	conState_t cState;
	uint8_t exit;
		
	uint8_t* dlBuffer; //download buffer
	uint32_t dlbufsiz;

	uint8_t curPageType; //current page type
	
	//if in directory
	uint32_t pageNumLines; //current directory number of lines
	WTG_Line_t* lines;
	
	uint32_t curSelLine; //currently selected line
	
	unsigned char* usrMessage; //user input message
	
}sessState;

void WTG_MSG(WTG_Error_t errorT);

int WTG_SetupSocket(conState_t* cs);
int WTG_SetupConnection(conState_t* cs, char* url, uint16_t port);
int WTG_Connect(conState_t* cs);
int WTG_InitSession(struct session* sess);
int WTG_EndSession(struct session* sess);
int WTG_Recv(struct session* sess);
uint32_t WTG_ParseDirectory(struct session* sess);

int WTG_MainLoop(void);
#endif