#include "tinyparse.h"

int8_t GOPH_parse_dir(size_t buffer_size, char* buffer, GOPH_page_t* page)
{
	GOPH_line_t** lines = &(page->lines);
	page->type = '1';

	size_t i = 0;
	size_t i0 = 0;
	size_t line_len = 0;
	size_t nlines = 0;

	int8_t lineParserRet = 0;
	GOPH_line_t lineTMP;

	// find \n position
	// If reached end of buffer and no \n was found, do the last parsing and
	// exit
	for (i=0; i < buffer_size; i++) {
		
		if (buffer[i] == '\n') {
			line_len = i - i0;
		} else if (i == buffer_size - 1) {
			line_len = i - i0 + 1;
		} else {
			continue;
		}
		
		GOPH_line_init(&lineTMP);
		lineParserRet = GOPH_line_parser(buffer + i0, line_len, &lineTMP);

		// Salta por cima do \n
		i0 = i + 1;

		if (lineParserRet == 0) {
			//FIXME: Maybe the page should have a push functionality, with
			// capacity and length.
			nlines++;

			*lines = (GOPH_line_t*) realloc(*lines,
											nlines*sizeof(GOPH_line_t));
			if(*lines == NULL){
				fprintf(stderr, "ERROR: failed to allocate new line struct memory\n");
				break;
			}

			(*lines)[nlines - 1] = lineTMP;

		// If lineParserRet = 3, then line is "." => end of parsing.
		} else if (lineParserRet == 3) {
			// TODO: talvez um simples break resolva isto.
			page->nlines = nlines;
			return 0;

		} else if (lineParserRet == 4) {
			// If the input line is not in correct format, ignore line.
			godmon("ERROR: lineParserRet = 4\n");
		}
	}

	page->nlines = nlines;
	return 0;
}
