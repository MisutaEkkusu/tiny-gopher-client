#ifndef _TINYCACHE_H_
#define _TINYCACHE_H_

//FIXME: Actualizar a lista das funções
// Verificar as dependências entre as funções.
// Estabelecer layers de dependências. As funções do mesmo layer não podem
// depender entre si. As funções só podem depender dos layers imediatamente
// abaixo?

#include "tinytypes.h"
#include "tinydebug.h"

// DEFAULT VALUES
// 4 Megabytes
static const size_t GOPH_CACHE_SIZE_MAX  = 0x400000;
static const size_t GOPH_CACHE_BLOCK_CAP = 16;

/*
// TESTING
static const size_t  GOPH_CACHE_SIZE_MAX      = 50000;
static const int32_t GOPH_CACHE_BLOCK_LEN_MAX = 2;
*/

typedef struct GOPH_cache_block{
	size_t cap;
	size_t len;
	size_t size;
	int32_t next;
	GOPH_line_t* lines;
	GOPH_page_t* pages;
} GOPH_cache_block_t;


typedef struct GOPH_cache{
	size_t size_max;
	size_t size;
	size_t len;
	size_t cap;
	int32_t first;
	int32_t last;
	GOPH_cache_block_t* blocks;
} GOPH_cache_t;


// BLOCKS

// ^GOPH_line_free();
// ^GOPH_page_free();
int8_t GOPH_cache_block_free(GOPH_cache_block_t* block);

// ^GOPH_line_free();
// ^GOPH_page_free();
int8_t GOPH_cache_block_empty(GOPH_cache_block_t* block);

int8_t GOPH_cache_block_initFull(GOPH_cache_block_t* block, size_t capacity);

int8_t GOPH_cache_block_addPage(GOPH_cache_block_t* block, GOPH_line_t* line,
                                GOPH_page_t* page);


// CACHE

int8_t GOPH_cache_free(GOPH_cache_t* cache);

int8_t GOPH_cache_init(GOPH_cache_t* cache);

int8_t GOPH_cache_reset(GOPH_cache_t* cache);

GOPH_cache_t GOPH_cache_new();

int8_t GOPH_cache_config(GOPH_cache_t* cache, size_t cache_size_max);



// CACHE BLOCKS MANIPULATION

// DEPENDS:
// GOPH_cache_block_free()
// GOPH_cache_block_initFull()
int8_t GOPH_cache_setCapacity(GOPH_cache_t* cache, size_t newCap);

// DEPENDS:
// GOPH_cache_block_empty()
int8_t GOPH_cache_removeBlock(GOPH_cache_t* cache, int32_t index);

// DEPENDS:
// GOPH_cache_setCapacity()
int8_t GOPH_cache_removeTopEmptyBlocks(GOPH_cache_t* cache);



// CACHE PAGES MANIPULATION

// Finds the address of a matching page
// DEPENDS:
// GOPH_line_equal()
int8_t GOPH_cache_findPage(GOPH_cache_t* cache, GOPH_line_t* line,
                           int32_t* foundBlock, int32_t* foundBIndex,
						   GOPH_page_t** foundPage);

// ^GOPH_cache_loadPage()
int8_t GOPH_cache_hasPage(GOPH_cache_t* cache, GOPH_line_t* line);

// ^GOPH_cache_loadPage()
GOPH_page_t GOPH_cache_loadPage(GOPH_cache_t* cache, GOPH_line_t* line);



// ^GOPH_line_size()
// ^GOPH_page_size()
// ^GOPH_cache_removeBlock()
// ^GOPH_cache_setCapacity()
// ^GOPH_cache_block_removePage()
int8_t GOPH_cache_savePage(GOPH_cache_t* cache, GOPH_line_t* line,
                           GOPH_page_t* page);

// ^GOPH_line_size()
// ^GOPH_page_size()
// ^GOPH_cache_findPage()
// ^GOPH_cache_block_removePage()
// ^GOPH_cache_removeBlock();
// ^GOPH_cache_removeTopEmptyBlocks();
int8_t GOPH_cache_removePage(GOPH_cache_t* cache, GOPH_line_t* line);



// PRINT

int8_t GOPH_cache_debug(GOPH_cache_t* cache);

#endif
