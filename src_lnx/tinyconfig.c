#include "tinyconfig.h"

// Default config
GOPH_config_t GOPH_config_new()
{	
	// FIXME: Os defaults devem ser postos aqui ou na bibliteca apropriada?
	GOPH_config_t config;

	char* homepage = "gopher://gopher.floodgap.com/";
	config.homepage = malloc(strlen(homepage) + 1);
	strcpy(config.homepage, homepage);

	config.cache_size_max = 0x400000;
	config.cache_block_len_max = 16;
	return config;
}

int8_t GOPH_config_free(GOPH_config_t* config)
{
	free(config->homepage);
	return 0;
}

// O ficheiro de configuração é muito simples. Cada linha é uma configuração
// organizada como key:value\n
//FIXME: Isolar parte disto como uma função de leitura de ficheiros ini.
// Isolar a função de verificação e cópia da string.
int8_t GOPH_config_readFile(GOPH_config_t* config)
{
	const char* configFn = "config/config.ini";

	int32_t i, j;
	int32_t key_pos, key_len;
	int32_t sep_pos;
	int32_t value_pos, value_len;
	
	int32_t str_pos, str_len;
	char* str_end_ptr;

	struct stat statbuf;
	if (stat(configFn, &statbuf)) {
		return 1;
	}

	FILE* fp = fopen(configFn, "r");
	char* buff = NULL;
	size_t buffSize = 0;
	ssize_t nChar = 0;

	while(1) {
		nChar = getline(&buff, &buffSize, fp);
		if (nChar == -1) {
			break;
		}

		//FIXME: Preciso de 3 marcadores: inicio da key, tamanho da key

		// Find first non whitespace
		for (i=0; i<nChar; i++) {
			if ((buff[i] != ' ') && (buff[i] != '\t')) {
				key_pos = i;
				j = i;
				break;
			}
		}


		// Check if is comment
		if (buff[key_pos] == '#') {
			continue;
		}
		

		// Find end of key
		key_len = 0;
		sep_pos = -1;
		for (i=j; i<nChar; i++) {
			if (buff[i] == '=') {
				sep_pos = i;
				j=i;
				break;
			}
			if ((buff[i] == ' ') || (buff[i] == '\t')) {
				j = i;
				break;
			}
			key_len++;
		}

		// Go to next non-whitespace
		if (sep_pos == -1) {
			for (i = j; i<nChar; i++) {
				if ((buff[i] != ' ') && (buff[i] != '\t')) {
					break;
				}
			}
			// If there is something between the key and the separator, then
			// this key is invalid.
			if (buff[i] != '=') {
				continue;
			}
			sep_pos = i;
			j = i;
		}

		// At this point, the caracter in position j must be a '='
		// The value is at the next non-whitespace
		for (i=j+1; i<nChar; i++) {
			if ((buff[i] != ' ') && (buff[i] != '\t')) {
				value_pos = i;
				j = i;
				break;
			}
		}
		
		// Get value length
		//FIXME: Talvez seja necessário arranjar uma função mais apropriada para
		// as strings
		for (i=j; i<nChar; i++) {
			if ((buff[i] == ' ') || (buff[i] == '\t')) {
				break;
			}
			value_len++;
		}


		if (!strncmp(buff + key_pos, "cache_size_max", key_len)) {
			config->cache_size_max = atol(buff + value_pos);

		} else if (!strncmp(buff + key_pos, "cache_block_len_max", key_len)) {
			config->cache_block_len_max = atoi(buff + value_pos);
		
		} else if (!strncmp(buff + key_pos, "homepage", key_len)) {
			// Verificar se começa numa aspa
			if (buff[value_pos] != '"') {
				continue;
			}
			str_pos = value_pos + 1;

			// Determinar onde está a outra aspa
			str_end_ptr = strchr(buff + str_pos, '"');
			if (!str_end_ptr) {
				continue;
			}
			str_len = str_end_ptr - (buff + str_pos);

			// Verificar se o resto da string é whitespace
			for (i=str_pos + str_len + 1; i<nChar; i++) {
				if ((buff[i] != ' ') || (buff[i] != '\t') ||
				    (buff[i] != '\n')) {
					continue;
				}
			}

			// Neste ponto, temos uma string válida
			free(config->homepage);
			config->homepage = malloc(str_len + 1);
			strncpy(config->homepage, buff + str_pos, str_len);
			config->homepage[str_len] = '\0';
		
		} else {
			godmon("config: option not found\n");
		}
	}

	// Close file
	fclose(fp);

	// Memory cleanup
	free(buff);

	return 0;
}



int8_t GOPH_config_readArgv(GOPH_config_t* config, int argc, char* argv[])
{
	if (argc != 2) {
		return 0;
	}

	free(config->homepage);
	//config->homepage = argv[1];
	config->homepage = malloc(strlen(argv[1])+ 1);
	strcpy(config->homepage, argv[1]);
	return 0;
}
