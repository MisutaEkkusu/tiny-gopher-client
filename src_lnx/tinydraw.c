#include "tinydraw.h"

// DRAW UTILS

//FIXME: funcao inacabada
// NOTA: as coordenadas começam no 1
int8_t GOPH_draw_text(int32_t row, int32_t col, char* msg)
{
	// Memorize initial position
	printf("\e7");
	// Go to position in screen
	printf("\e[%d;%dH", row, col);
	// Print message
	printf("%s", msg);
	// Return to initial position
	printf("\e8");
	return 0;
}


int8_t GOPH_draw_lineH(int32_t termW, int32_t row, char* c)
{
	// Memorize initial position
	printf("\e7");
	// Go to position in screen
	printf("\e[%d;%dH", row, 0);
	// Print row
	int i;
	for (i=0; i<termW; i++) {
		fputs(c, stdout);
		//putchar(c);
	}
	// Return to initial position
	printf("\e8");
	return 0;
}


int8_t GOPH_draw_stripH(int32_t width, int32_t col, int32_t row, char* c)
{
	// Memorize initial position
	printf("\e7");
	// Go to position in screen
	printf("\e[%d;%dH", row, col);
	// Print row
	int i;
	for (i=0; i<width; i++) {
		fputs(c, stdout);
		//putchar(c);
	}
	// Return to initial position
	printf("\e8");
	return 0;
}


int8_t GOPH_draw_textLineWithBG(int32_t row, int32_t col, char* msg,
                                char* bgChar)
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	int32_t H = ws.ws_row;
	int32_t W = ws.ws_col;

	// If the inputs are negative, it means counting from finish
	if (row < 0) {
		row = (H + row)%H + 1;
	}

	if (col < 0) {
		col = (W + col)%W + 1;
	}

	int32_t msg_len = strlen(msg);

	GOPH_draw_stripH(col-1, 1, row, bgChar);
	GOPH_draw_text(row, col, msg);
	GOPH_draw_stripH(W - (col - 1) - msg_len, col + msg_len, row, bgChar);

	// Flush terminal
	fflush(stdout);

	return 0;
}
