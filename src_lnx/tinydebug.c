#include "tinydebug.h"


const char* tinyfifo = "/tmp/gopher-debug-pipe";

int godmon_init(){
	// Deactivates the SIGPIPE signal, avoiding program exit on pipe
	// failure.
	signal(SIGPIPE, SIG_IGN);
	return 0;
}

// FIXME: Esta função só deveria abrir o pipe e escrever uma string de entrada.
int godmon_print(char* tag, char* msg_fmt, va_list args)
{
	FILE* fd;

	//FIXME: Antes de usar o pipe, tenho de verificar duas coisas:
	// 1) Se o ficheiro existe
	// 2) Se o ficheiro é um pipe (posso agrupar este passo com o anterior
	// 3) Se tenho permissão para escrever
	// Além disso, tenho de evitar um SIGPIPE duarante as verificações.
	// NOTA: só o programa monitor é que cria ou apaga pipes, portanto se
	// este não existir, assume-se que não há monitor e portanto não se
	// manda nada.

	// 1) Check if file exists;
	struct stat sb;
	if (stat(tinyfifo, &sb)) return 1;

	// 2) Check if is a pipe
	// "The macro shall evaluate to a non-zero value if the test is true;
	// 0 if the test is false."
	if (!S_ISFIFO(sb.st_mode)) return 2;

	fd = fopen(tinyfifo, "w");
	if (fd == NULL) return 3;

	if (tag != NULL) {
		if (!fprintf(fd, "[%s] ", tag)) return 4;
	}

	if (!vfprintf(fd, msg_fmt, args)) return 5;
	fclose(fd);

	//HACK: tempo de espera para o pipe conseguir trabalhar como deve ser
	//FIXME: Acho que o problema está relacionado com o pipe não estar
	// disponível para escrita, o que cria um SIGPIPE e mata o programa.

	// A partir de 100 us para baixo, os erros começam a surgir
	// Não posso escrever um \n extra no godmon_monitor, ele tem que ir
	// logo na mensagem.
	// Assim, posso simplemente eliminar o sleep.

	// Pensando melhor, vou voltar a meter um sleep de 100us.
	// O outup do monitor tá a aparecer com algumas linhas trocadas ou mesmo
	// eliminadas
	usleep(100);
	return 0;
}


int godmon(char* msg_fmt, ...) {
	va_list args;
	va_start(args, msg_fmt);
	int ret = godmon_print(NULL, msg_fmt, args);
	va_end(args);
	return ret;
}


int godmontag(char* tag, char* msg_fmt, ...) {
	va_list args;
	va_start(args, msg_fmt);
	int ret = godmon_print(tag, msg_fmt, args);
	va_end(args);
	return ret;
}


//FIXME: Não há gestão de memória. Não há maneira soft de sair, só com ctrl+c
int godmon_monitor(){
	
	int buffer_len = 256;
	char* buffer = malloc(buffer_len);

	FILE* fd;

	mkfifo(tinyfifo, 0666);
	
	size_t in_bytes = 0;

	while (1){
		fd = fopen(tinyfifo, "r");
		while((in_bytes = fread(buffer, 1, buffer_len, fd))) {
			fwrite(buffer, 1, in_bytes, stdout);
		}
		fflush(stdout);
		fclose(fd);
	}

	return 0;
}


// Monitor cleanup after a ctrl+c
void godmon_monitor_exit()
{
	// BRUTE PIPE CLEANUP
	//FIXME: Does not check if pipe is still being read
	struct stat sb;
	if (!stat(tinyfifo, &sb)) remove(tinyfifo);
	
	printf("\nEND GOPHER MONITOR\n");
	exit(0);
}
