#include "tinyfile.h"


int8_t GOPH_get_file(char* filename, GOPH_page_t* page){
	size_t buffer_len = 0;
	char* buffer = NULL;

	page->lines  = NULL;
	page->nlines = 0;

	FILE* fp = fopen(filename , "r");
	
	uint32_t n = 0;
	while(1) {
		getline(&buffer, &buffer_len, fp);

		if (feof(fp)){
			break;
		}
		
		// FIXME: Deviam ser reallocs de ver em quando e não linha a linha.
		// Podia contar as linhas primeiro.
		page->lines = realloc(page->lines, (n + 1)*sizeof(GOPH_line_t));
		
		if (GOPH_line_parser(buffer, buffer_len, &((page->lines)[n]))) break;

		// Increase line count
		n++;
		
		// Update page nlines
		page->nlines = n;
	}

	fclose(fp);
	free(buffer);

	return 0;
}


int8_t GOPH_get_file_txt(char* filename, GOPH_page_t* page){

	FILE* fp = fopen(filename , "r");
	fseek(fp, 0, SEEK_END);
	uint32_t n = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char* buffer = malloc(n);
	fread(buffer, 1, n, fp);

	fclose(fp);

	page->buffer_size = n;
	page->buffer = buffer;

	page->type = '0';
	
	return 0;
}
