#include "tinyhist.h"


// HIST STRUCTURE

int8_t GOPH_hist_init(GOPH_hist_t* hist)
{
	hist->len = 0;
	hist->cap = 0;
	hist->pos = 0;
	hist->ptr = NULL;
	return 0;
}


int8_t GOPH_hist_free(GOPH_hist_t* hist)
{
	//First deallocate everything
	int32_t i;
	for(i = 0; i < hist->len; i++) {
		GOPH_state_free(hist->ptr + i);
	}
	free(hist->ptr);
	return 0;
}


GOPH_hist_t GOPH_hist_new()
{
	GOPH_hist_t hist;
	GOPH_hist_init(&hist);
	return hist;
}


int8_t GOPH_hist_reset(GOPH_hist_t* hist)
{
	GOPH_hist_free(hist);
	GOPH_hist_init(hist);
	return 0;
}


// O estado tem de ser copiado
int8_t GOPH_hist_push(GOPH_hist_t* hist, GOPH_state_t* state)
{
	const int32_t cap_delta = 16;

	if (hist->len == hist->cap) {
		hist->cap += cap_delta;
		hist->ptr = realloc(hist->ptr, hist->cap * sizeof(GOPH_state_t));
	}
	
	// Clone input state
	hist->ptr[hist->len] = GOPH_state_clone(state);
	hist->len += 1;

	return 0;
}


int8_t GOPH_hist_pop(GOPH_hist_t* hist, GOPH_state_t* state)
{
	const int32_t cap_delta = 16;
	if (hist->len == 0) return 1;

	if (state != NULL) {
		*state = (hist->ptr)[hist->len-1];
	} else {
		GOPH_state_free(hist->ptr + hist->len - 1);
	}

	hist->len -= 1;

	if (hist->pos >= hist->len) {
		hist->pos = hist->len - 1;
	}

	if (hist->len == (hist->cap - cap_delta)){
		hist->ptr = realloc(hist->ptr, hist->len * sizeof(GOPH_state_t));
		hist->cap -= cap_delta;
	}

	return 0;
}


int8_t GOPH_hist_debug(GOPH_hist_t* hist)
{
	int32_t i;
	godmontag("hist", "len: %i\n", hist->len);
	godmontag("hist", "cap: %i\n", hist->cap);
	godmontag("hist", "pos: %i\n", hist->pos);
	for(i=0; i<hist->len; i++){
		godmontag("hist", "%i %s\n", i, (hist->ptr)[i].page_line.disp);
	}
	return 0;
}



// HIST MANIPULATION

int8_t GOPH_hist_getStateFromPage(GOPH_page_t*  page, GOPH_state_t* state_in,
                                  GOPH_state_t* state_out)
{
	int32_t link_n = state_in->link_n;
	GOPH_state_reset(state_out);

	// link_n must be non-negative
	if (link_n < 0) {
		return 1;
	}

	// Search the line corresponding to link_n
	int32_t link_line_pos = 0;
	int32_t link_count = 0;
	for(int32_t i=0; i < page->nlines; i++) {
		if (!GOPH_line_isLink(page->lines + i)) {
			continue;
		}
		link_count++;
		if (link_count-1 == link_n) {
			link_line_pos = i;
			break;
		}
	}

	// The page has no links
	if (link_count == 0) {
		return 1;
	}

	// link_n exceeds the number of links
	if (link_n >= link_count) {
		return 1;
	}

	// Standard case
	state_out->page_line = GOPH_line_clone(page->lines + link_line_pos);
	GOPH_debug_line(&(state_out->page_line));

	return 0;
}


int8_t GOPH_hist_updateState(GOPH_hist_t* hist, ssize_t pos,
                             GOPH_state_t* state)
{
	if (pos < 0 || pos >= hist->len) {
		return 1;
	}
	(hist->ptr)[pos].screen_top = state->screen_top;
	(hist->ptr)[pos].link_n     = state->link_n;
	return 0;
}


int8_t GOPH_hist_addTopState(GOPH_hist_t* hist, GOPH_state_t* state)
{
	GOPH_hist_push(hist, state);
	hist->pos = hist->len - 1;
	return 0;
}


// Vou carregar o estado anterior no state_io ?
int8_t GOPH_hist_stepForw(GOPH_hist_t* hist, GOPH_state_t* state_io)
{
	if (hist->pos == hist->len - 1) {
		return 1;
	}
	hist->pos++;

	GOPH_state_free(state_io);
	*state_io = GOPH_state_clone(hist->ptr + hist->pos);

	return 0;
}


int8_t GOPH_hist_stepBack(GOPH_hist_t* hist, GOPH_state_t* state_io)
{
	if (hist->pos == 0) {
		return 1;
	}
	hist->pos--;

	GOPH_state_free(state_io);
	*state_io = GOPH_state_clone(hist->ptr + hist->pos);

	return 0;
}

//TODO: arranjar um nome decente
int8_t GOPH_hist_removeAllAbovePos(GOPH_hist_t* hist)
{
	ssize_t i;
	ssize_t n_pops = hist->len - 1 - hist->pos;
	for(i = 0; i < n_pops; i++) {
		if (GOPH_hist_pop(hist, NULL)) {
			break;
		}
	}
	return 0;
}
