#include "tinycache.h"


//FIXME: Determinar quem é que manda em que alocações.
// Quando desalocar a memoria e ponteiros e porque funções.


//FIXME: Rever a contabilidade dos size. O que considerar para o calculo de uma
// estrutura?


//FIXME: Remover funções redundantes. Condensar funções.
// Verificar, função a função, se é necessária.
// Clarificar o funcionamento das funções.


int8_t GOPH_cache_block_free(GOPH_cache_block_t* block)
{
	size_t i;

	for (i=0; i<block->len; i++) {
		GOPH_line_free(block->lines + i);
		GOPH_page_free(block->pages + i);
	}

	free(block->lines);
	free(block->pages);
	
	return 0;
}


// Free all lines and pages and resets the metadata.
// Does not free the lines and pages arrays.
int8_t GOPH_cache_block_empty(GOPH_cache_block_t* block)
{
	size_t i;

	for (i=0; i<block->len; i++) {
		GOPH_line_free(block->lines + i);
		GOPH_page_free(block->pages + i);
	}
	block->len  = 0;
	block->size = 0;
	block->next = -1;

	return 0;
}


// FIXME: Esta função não está minimalista/simples/ortogonal
// Está a inicializar valores e a alocar memória. Só deveria fazer uma coisa.
// Talvez precise de uma função setCapacity para os blocos?
int8_t GOPH_cache_block_initFull(GOPH_cache_block_t* block, size_t capacity)
{
	block->cap   = capacity;
	block->len   =  0;
	block->size  =  0;
	block->next  = -1;
	block->lines = malloc(capacity * sizeof(GOPH_line_t));
	block->pages = malloc(capacity * sizeof(GOPH_page_t));
	return 0;
}


int8_t GOPH_cache_block_addPage(GOPH_cache_block_t* block, GOPH_line_t* line,
                                GOPH_page_t* page)
{
	size_t newSize = GOPH_line_size(line) + GOPH_page_size(page);
	block->lines[block->len] = GOPH_line_clone(line);
	block->pages[block->len] = *page;
	block->len  += 1;
	block->size += newSize;
	return 0;
}


int8_t GOPH_cache_block_removePage(GOPH_cache_block_t* block, int32_t index)
{
	size_t oldSize = GOPH_line_size(block->lines + index) +
	                 GOPH_page_size(block->pages + index);
	
	// 1) Free alocations
	GOPH_line_free(block->lines + index);
	GOPH_page_free(block->pages + index);

	// 2) Move elements back
	size_t i;
	for (i=index; i<(block->len-1); i++) {
		block->lines[i] = block->lines[i+1];
		block->pages[i] = block->pages[i+1];
	}

	// 3) Update block metadata
	block->len  -= 1;
	block->size -= oldSize;

	return 0;
}


// CACHE

int8_t GOPH_cache_free(GOPH_cache_t* cache)
{
	size_t i;
	for (i=0; i<cache->cap; i++) {
		GOPH_cache_block_free(cache->blocks + i);
	}
	free(cache->blocks);
	return 0;
}


int8_t GOPH_cache_init(GOPH_cache_t* cache)
{
	cache->size_max =  GOPH_CACHE_SIZE_MAX;
	cache->len      =  0;
	cache->cap      =  0;
	cache->size     =  0;
	cache->first    = -1;
	cache->last     = -1;
	cache->blocks   = NULL;
	return 0;
}


int8_t GOPH_cache_reset(GOPH_cache_t* cache)
{
	GOPH_cache_free(cache);
	GOPH_cache_init(cache);
	return 0;
}


GOPH_cache_t GOPH_cache_new()
{
	GOPH_cache_t cache;
	GOPH_cache_init(&cache);
	return cache;
}

int8_t GOPH_cache_config(GOPH_cache_t* cache, size_t cache_size_max)
{
	cache->size_max = cache_size_max;
	return 0;
}




// BLOCK MANIPULATION

int8_t GOPH_cache_setCapacity(GOPH_cache_t* cache, size_t newCap)
{
	size_t i;
	size_t oldCap = cache->cap;

	if (newCap == oldCap) {
		return 0;
	}
	
	// REMOVE BLOCKS
	if (newCap < oldCap) {
		for (i = newCap; i<oldCap; i++) {
			// Update Cache
			cache->size -= cache->blocks[i].size;
			GOPH_cache_block_free(cache->blocks + i);
		}
	}
	
	// RESIZE BLOCKS ALOCATION
	cache->cap    = newCap;
	cache->blocks = realloc(cache->blocks,
	                        newCap * sizeof(GOPH_cache_block_t));
	
	
	// ADD BLOCKS
	if (newCap > oldCap) {
		for (i=oldCap; i<newCap; i++) {
			GOPH_cache_block_initFull(cache->blocks + i, GOPH_CACHE_BLOCK_CAP);
		}
	}
	
	return 0;
}


// Removes a block from the cache linked list.
int8_t GOPH_cache_removeBlock(GOPH_cache_t* cache, int32_t index)
{
	if (cache->len == 0) {
		return 1;
	}
	
	// 1) Obter os vizinhos
	int32_t k = cache->first;
	int32_t bPrev = -1;
	size_t i;
	for (i=0; i<cache->len; i++) {
		if (k == index) {
			break;
		}
		bPrev = k;
		k = cache->blocks[k].next;
	}
	int32_t bNext = cache->blocks[index].next;
	
	// Acertar os vizinhos
	if (index == cache->first) {
		cache->first = bNext;
	} else {
		cache->blocks[bPrev].next = bNext;
	}

	// Acertar a cache
	cache->len  -= 1;
	cache->size -= cache->blocks[index].size;
	if (cache->len == 0) {
		cache->last = -1;
	}

	GOPH_cache_block_empty(cache->blocks + index);

	return 0;
}



int8_t GOPH_cache_removeTopEmptyBlocks(GOPH_cache_t* cache)
{
	int32_t i = cache->cap;
	
	if (i==0) {
		return 0;
	}
	
	int32_t c = 0;
	while(i--) {
		if (cache->blocks[i].len != 0){
			break;
		}
		c++;
	}
	
	// Resize cache
	GOPH_cache_setCapacity(cache, cache->cap - c);
	
	return 0;
}



int8_t GOPH_cache_findPage(GOPH_cache_t* cache, GOPH_line_t* line,
                           int32_t* foundBlock, int32_t* foundBIndex,
						   GOPH_page_t** foundPage)
{
	size_t i, j;
	size_t k  = cache->first;
	size_t len = cache->len;
	for (i=0; i<len; i++) {
		for (j=0; j<cache->blocks[k].len; j++) {
			if (GOPH_line_equal(cache->blocks[k].lines + j, line)) {
				*foundPage   = cache->blocks[k].pages + j;
				*foundBlock  = k;
				*foundBIndex = j;
				return 1;
			}
		}
		k = cache->blocks[k].next;
	}
	*foundPage = NULL;
	return 0;
}


// Checks is page is in cache
int8_t GOPH_cache_hasPage(GOPH_cache_t* cache, GOPH_line_t* line)
{
	GOPH_page_t* dummy_page;
	int32_t      dummy_block;
	int32_t      dummy_bIndex;
	return GOPH_cache_findPage(cache, line, &dummy_block, &dummy_bIndex,
	                           &dummy_page);
}

// Loads a page from cache
GOPH_page_t GOPH_cache_loadPage(GOPH_cache_t* cache, GOPH_line_t* line)
{
	GOPH_page_t* page;
	int32_t      dummy_block;
	int32_t      dummy_bIndex;
	GOPH_cache_findPage(cache, line, &dummy_block, &dummy_bIndex, &page);
	GOPH_page_t page_clone = GOPH_page_clone(page);
	return page_clone;
}

// Saves a page into cache
// The page is copied into the cache
int8_t GOPH_cache_savePage(GOPH_cache_t* cache, GOPH_line_t* line,
                           GOPH_page_t* page)
{
	// 1) Verify if the page fits into the cache
	size_t newSize = GOPH_line_size(line) + GOPH_page_size(page);
	
	if (newSize > cache->size_max) {
		return 1;
	}
	

	// 2) Free space to add the new page
	size_t freeSpace = cache->size_max - cache->size;
	while (freeSpace < newSize) {
		freeSpace += cache->blocks[cache->first].size;
		GOPH_cache_removeBlock(cache, cache->first);
	}
	
	// 2.1) Optimize cache
	GOPH_cache_removeTopEmptyBlocks(cache);


	// 3) Get last block index
	int32_t lastBlock = -1;
	// 3.1) Check if last block can accept new entries
	size_t i;
	if ((cache->last == -1) ||
	    (cache->blocks[cache->last].len == cache->blocks[cache->last].cap)) {

		if (cache->len == cache->cap) {
			GOPH_cache_setCapacity(cache, cache->cap + 1);
			lastBlock = cache->len;
		} else {
			for (i=0; i<cache->cap; i++) {
				if (cache->blocks[i].len == 0) {
					lastBlock = i;
					break;
				}
			}
		}
		
		// Update cache structure with new len and if needed, new cap.
		cache->len += 1;
		// Edge case
		if (cache->len > 1) {
			cache->blocks[cache->last].next = lastBlock;
		}
		cache->last = lastBlock;
	} else {
		lastBlock = cache->last;
	}

	// 4) Add new entry (page clone)
	GOPH_page_t page_clone = GOPH_page_clone(page);
	GOPH_cache_block_addPage(cache->blocks + lastBlock, line, &page_clone);

	
	// 5) Update Cache
	cache->size += newSize;
	// Edge Case
	if (cache->first == -1) {
		cache->first = lastBlock;
	}
	
	return 0;
}


int8_t GOPH_cache_removePage(GOPH_cache_t* cache, GOPH_line_t* line)
{
	GOPH_page_t* page;
	int32_t bIndex;
	int32_t index;

	// 1) Search for page
	if (!GOPH_cache_findPage(cache, line, &bIndex, &index, &page)) {
		return 1;
	}

godmontag("cache", "removePage() page found. b:%d i:%d\n", bIndex, index);
	
	// 2) Calcultate alocations size
	size_t oldSize = GOPH_line_size(cache->blocks[bIndex].lines + index) +
	                 GOPH_page_size(cache->blocks[bIndex].pages + index);

godmontag("cache", "removePage() before block_removePage\n");
	GOPH_cache_block_removePage(cache->blocks + bIndex, index);
godmontag("cache", "removePage() after block_removePage\n");
	
	// 3) Cache Update
	cache->size -= oldSize;
	if (cache->blocks[bIndex].len == 0) {
		GOPH_cache_removeBlock(cache, bIndex);
	}

	// 4) Optimization
	GOPH_cache_removeTopEmptyBlocks(cache);

godmon("\n");
	return 0;
}


int8_t GOPH_cache_debug(GOPH_cache_t* cache)
{	
	godmontag("cache", "len:%d   cap:%d   size:%d\n",
	       cache->len, cache->cap, cache->size);
	godmontag("cache", "first:%d   last:%d\n", cache->first, cache->last);

	size_t i;
	godmontag("cache", "next: ");
	for (i=0; i<cache->cap; i++) {
		godmon("%d ", cache->blocks[i].next);
	}

	godmon("\n");
	godmontag("cache", "len: ");
	for (i=0; i<cache->cap; i++) {
		godmon("%d ", cache->blocks[i].len);
	}
	godmon("\n\n");
	return 0;
}
