#ifndef _TINYCONFIG_H
#define _TINYCONFIG_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <string.h>

#include "tinydebug.h"

typedef struct GOPH_config_t {
	size_t cache_size_max;
	size_t cache_block_len_max;
	char* homepage;
} GOPH_config_t;


// Default config
GOPH_config_t GOPH_config_new();

int8_t GOPH_config_free(GOPH_config_t* config);

int8_t GOPH_config_readFile(GOPH_config_t* config);

int8_t GOPH_config_readArgv(GOPH_config_t* config, int argc, char* argv[]);

#endif
