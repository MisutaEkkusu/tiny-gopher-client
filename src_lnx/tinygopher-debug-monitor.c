#include "tinydebug.h"
#include <signal.h>

int main(){
	
	// ctrl+c handling
	struct sigaction act;
	act.sa_handler = godmon_monitor_exit;
	sigaction(SIGINT, &act, NULL);
	
	printf("\n////////////// TINY GOPHER DEBUG MONITOR ////////////////\n");
	printf("       use godmon(\"format\", ...) to show output here.\n\n");
	
	godmon_monitor();

	return 0;
}
