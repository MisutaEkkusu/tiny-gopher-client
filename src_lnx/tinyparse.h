#ifndef _TINYPARSER_H_
#define _TINYPARSER_H_

//#include <stdio.h>

#include "tinytypes.h"
#include "tinydebug.h"

int8_t GOPH_parse_dir(size_t buffer_size, char* buffer, GOPH_page_t* page);

#endif
