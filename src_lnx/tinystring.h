#ifndef _TINYUTILS_H
#define _TINYUTILS_H

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>

// Dump a message to stdout
void say(char* msg);

// Builds a allocaded string from the input array
char* GOPH_str_new(char* str);

// Calculates the number of chars of the string representation
size_t GOPH_str_i32_len(int32_t num);

#endif
