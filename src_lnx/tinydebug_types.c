#include "tinydebug_types.h"

int8_t GOPH_debug_line(GOPH_line_t* line)
{
	godmontag("debug", "gopher://%s/%c%s\n", line->dom, line->type, line->sel);
	godmontag("debug", "type: %c\n", line->type);
	godmontag("debug", "disp: %s\n", line->disp);
	godmontag("debug", "sel : %s\n", line->sel);
	godmontag("debug", "dom : %s\n", line->dom);
	godmontag("debug", "port: %u\n", line->port);

	return 0;
}

int8_t GOPH_debug_state(GOPH_state_t* state)
{
	godmontag("debug", " page_line: %s\n", state->page_line.disp);
	godmontag("debug", "screen_top: %i\n", state->screen_top);
	godmontag("debug", "    link_n: %i\n", state->link_n);
	return 0;
}
