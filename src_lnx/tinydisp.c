#include "tinydisp.h"
#include "tinydebug.h"


int8_t GOPH_render_init(GOPH_render_t* render){
	render->normal_list = GOPH_list_str_new();
	render->link_list   = GOPH_list_str_new();
	render->nlinks = 0;
	render->link_location_page = NULL;
	render->link_location      = NULL;
	render->link_nlines        = NULL;
	return 0;
}


int8_t GOPH_render_free(GOPH_render_t* render){
	GOPH_list_str_free(&(render->normal_list));
	GOPH_list_str_free(&(render->link_list));
	render->nlinks = 0;
	free(render->link_location_page);
	free(render->link_location);
	free(render->link_nlines);
	return 0;
}


GOPH_render_t GOPH_render_new(){
	GOPH_render_t render;
	GOPH_render_init(&render);
	return render;
}


int8_t GOPH_render_reset(GOPH_render_t* render){
	GOPH_render_free(render);
	GOPH_render_init(render);
	return 0;
}


//FIXME: inacabada
int8_t GOPH_render_dump(GOPH_render_t* render){
	int32_t i;
	int32_t normal_list_len = render->normal_list.len;
	printf("normal_list.len: %i\n", normal_list_len);
	printf("normal_list.list:\n");
	for (i=0; i<normal_list_len; i++) {
		printf("%s\n", render->normal_list.list[i]);
	}
	return 0;
}


int8_t GOPH_render_debug_dump(GOPH_render_t* render){
	int32_t i;
	int32_t normal_list_len = render->normal_list.len;
	godmon("normal_list.len: %i\n", normal_list_len);
	godmon("normal_list.list:\n");
	for (i=0; i<normal_list_len; i++) {
		godmon("%s\n", render->normal_list.list[i]);
	}
	return 0;
}




/// RENDER ENGINE ////


int32_t GOPH_get_screen_width()
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	return ws.ws_col;
}

int32_t GOPH_get_screen_height()
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	return ws.ws_row;
}


int8_t GOPH_line_render(char code, uint8_t has_bg, uint8_t has_prefix,
                        char* line, char* buffer, int32_t line_len)
{
	// Prefixs
	// 0123456789+gIThis

	uint8_t n_formats = 17;

	char* form_pre[] = {
		"(FILE)",	// 0
		" (DIR)",	// 1
		" (CSO)",	// 2
		" (ERR)",	// 3
		"(MACB)",	// 4
		"(DOSB)",	// 5
		"(UNUU)",	// 6
		" (VER)",	// 7 : search
		"(TELN)",	// 8
		" (BIN)",	// 9
		"   (+)",	// +
		" (GIF)",	// g
		" (IMG)",	// I
		"(TELN)",	// T
		"(HTTP)",	// h
		"      ",	// i
		" (SND)",	// s
		"   (?)",	// other
	};
	
	uint8_t color = 0;
	switch(code)
	{
		case '0': color = 31; break;
		case '1': color = 34; break;
		case 'h': color = 32; break;
	}


	// FIXME: alterar para um modo mais eficiente de procura.
	// Os tipos mais usados são 0, 1, i (principalmente i)
	const char* prelist = "0123456789+gIThis";
	// Valor default (i)
	size_t i = 15;
	
	if (has_prefix) {
		
		switch(code) {
			case 'i':
				i = 15;
				break;
			case '0':
				i = 0;
				break;
			case '1':
				i = 1;
				break;
			default:
				for(i=0; i<n_formats; i++) {
					if (prelist[i] == code) {
						break;
					}
				}
		}
	}
	

	int32_t print_len = 0;

	if (color) {
		// +15 chars = 6(prefix) + 1(espaco) + 7(cor) + 1(\0)
		print_len = line_len + 15;
		
		if (has_bg) {
			// Introdução do código de inversão de cores (\e[7m)
			// +4 chars (inversao)
			print_len += 4;
			snprintf(buffer, print_len, "%s \e[7m\e[1;%im%s", form_pre[i],
			         color, line);
		} else {
			snprintf(buffer, print_len, "%s \e[1;%im%s", form_pre[i],
			         color, line);
		}
		//Print dos caracteres finais
		// +5 chars = 4(cor) + 1(\0)
		snprintf(buffer + print_len - 1, 5, "\e[0m");
	}else{
		// +8 chars = 6(prefix) + 1(espaco) + 1(\0)
		print_len = line_len + 8;
		snprintf(buffer, print_len, "%s %s", form_pre[i], line);
	}

	return 0;
}




int8_t GOPH_dir_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render)
{
	//FIXME: Rever a contabilidade dos caracteres

	// É preciso descontar as 7 colunas das esquerda.
	int32_t screen_w = GOPH_get_screen_width() - 7;
	
	int32_t nlines = GOPHER_Page->nlines;
	
	//// Buffer de comunicação com o render_lines ////
	// Cada linha tem no máximo screen_w caracteres no ecrã.
	// Se cada caracter for um emoji, consome 4 bytes em UTF8 (4*screen_w B).
	// Mais os bytes de configuração (10B antes + 4B depois).
	// Mais o endline e o null (2B).
	// Dá 4*screen_w + 16 B
	char* buffer = malloc(4*screen_w + 16);
	

	char type;
	char* line;
	int32_t i;
	int32_t line_size;
	int32_t j, j0;

	uint8_t has_prefix;

	// Cache para o segundo loop
	uint8_t*  is_link_list   = malloc(nlines * sizeof(uint8_t));
	int32_t* line_size_list = malloc(nlines * sizeof(int32_t));
	uint8_t is_link = 0;
	int32_t nlinks = 0;
	int32_t render_location = 0;

	for (i=0; i<nlines; i++){
		line = GOPHER_Page->lines[i].disp;
		type = GOPHER_Page->lines[i].type;
		line_size = strlen(line);
		
		// Check if is a link
		switch(type) {
			case '0':
				is_link = 1;
				break;;
			case '1':
				is_link = 1;
				break;;
			case 'h':
				is_link = 1;
				break;;
			default:
				is_link = 0;
		}
		
		// Caching values for next loop
		is_link_list[i]   = is_link;
		line_size_list[i] = line_size;
		
		// Link counting
		if (is_link) nlinks += 1;
	}

	//Render parameters
	render->nlinks = nlinks;

	//Render memory allocation
	render->link_location_page = realloc(render->link_location_page,
	                                     nlinks * sizeof(int32_t));
	render->link_location      = realloc(render->link_location,
	                                     nlinks * sizeof(int32_t));
	render->link_nlines        = realloc(render->link_nlines,
	                                     nlinks * sizeof(int32_t));

	for (i=0; i<nlinks; i++){
		render->link_nlines[i] = 0;
	}

	// Link index
	size_t li = 0;
	// Auxiliary variable
	int32_t k;
	int32_t j0_new;
	for (i=0; i<nlines; i++){
		
		has_prefix = 1;

		line = GOPHER_Page->lines[i].disp;
		type = GOPHER_Page->lines[i].type;
		is_link = is_link_list[i];
		line_size = line_size_list[i];

		if (is_link) {
			// location in page_t
			render->link_location_page[li] = i;
			// location in render
			render->link_location[li] = render_location;
		}

		// LINE CUTTER
		j0 = 0;
		j0_new = 0;
		j = 0;
		
		//FIXME: Faltam as palavras compridas
		while(1) {
			j += screen_w;
			j0_new = j;
			if (j>=line_size) break;

			// Search for the last space
			// If no space is found, just do a hard cut
			for(k=j; k>j0; k--){
				if (line[k] == ' ') {
					j = k;
					// Salta o espaço
					j0_new = k + 1;
					break;
				}
			}

			// Add new line to render
			GOPH_line_render(type, 0, has_prefix, line + j0, buffer, j - j0);
			GOPH_list_str_push(&(render->normal_list), buffer, strlen(buffer));
			
			// FIXME: estou a fazer push de linhas vazias. Tenho de evitar isto.
			if (is_link) {
				GOPH_line_render(type, 1, has_prefix, line + j0, buffer,
				                   j - j0);

				//printf("buffer: |%s|\n", buffer);

				GOPH_list_str_push(&(render->link_list), buffer, strlen(buffer));
				render->link_nlines[li] += 1;
			}else{
				GOPH_list_str_push(&(render->link_list), "", 0);
			}

			// Avanço do contador de linhas do render
			render_location += 1;

			j0 = j0_new;
			
			// Os próximos prints não têm prefixo.
			has_prefix = 0;
		}

		// Add last line to render
		GOPH_line_render(type, 0, has_prefix, line + j0, buffer, line_size - j0);
		GOPH_list_str_push(&(render->normal_list), buffer, strlen(buffer));
		
		// Final configurations for links
		// FIXME: estou a fazer push de linhas vazias. Tenho de evitar isto.
		if (is_link) {
			GOPH_line_render(type, 1, has_prefix, line + j0, buffer,
			                 line_size - j0);

			//printf("buffer: |%s|\n", buffer);

			GOPH_list_str_push(&(render->link_list), buffer, strlen(buffer));
			render->link_nlines[li] += 1;
			// Avanço do contador de links
			li += 1;
		}else{
			GOPH_list_str_push(&(render->link_list), "", 0);
		}

		// Avanço do contador de linhas do render
		render_location += 1;

	}

	free(is_link_list);
	free(line_size_list);
	free(buffer);

	return 0;
}


int8_t GOPH_txt_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render)
{
	// É preciso descontar as 7 colunas das esquerda.
	int32_t screen_w = GOPH_get_screen_width();
	
	int32_t txt_len = GOPHER_Page->buffer_size;
	char* txt = GOPHER_Page->buffer;
	
	int32_t i;
	int32_t i0 = 0;
	int32_t j = 0;

	// i0 não vai marcar a posição de um \n

	int32_t str_len = 0;

	for (i=0; i<txt_len; i++){

		if (txt[i] == '\n') {
			str_len = i-i0;
			if (i>0 && txt[i-1] == '\r') {
				str_len -= 1;
			}
			
			GOPH_list_str_push(&(render->normal_list), txt + i0, str_len);

			// i0 salta o \n
			i0 = i + 1;

			// Como o caracter actual (i) tem índice j inválido, então o próximo
			// caracter terá índice j=0
			j = 0;
			continue;
		}

		if (j==screen_w) {
			str_len = i-i0;
			GOPH_list_str_push(&(render->normal_list), txt + i0, str_len);
			i0 = i;
			// Como o caracter actual (i) tem índice j=0, então o próximo
			// caracter terá índice j=1
			j = 1;
			continue;
		}

		j++;
	}

	// Caso o ultimo caracter não seja um \n, então o último segmento do
	// texto ainda não foi guardado.
	if (txt_len && txt[txt_len-1] != '\n') {
		str_len = txt_len - i0;
		GOPH_list_str_push(&(render->normal_list), txt + i0, str_len);
	}
	
	return 0;
}


// Selects the appropriated algoritm to render the page.
int8_t GOPH_page_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render){
	char type = GOPHER_Page->type;
	if (type == '0') {
		GOPH_txt_render(GOPHER_Page, render);
	} else if (type == '1') {
		GOPH_dir_render(GOPHER_Page, render);
	}
	return 0;
}


// PAGER

int8_t GOPH_pagerState_init(GOPH_pagerState_t* state)
{
	state->text_h         =  0;
	state->render_pos     =  0;
	state->link_n         = -1;
	return 0;
}


GOPH_pagerState_t GOPH_pagerState_new()
{
	GOPH_pagerState_t state;
	GOPH_pagerState_init(&state);
	return state;
}


int8_t GOPH_disp_clear_block(int32_t screen_line, int32_t n_lines)
{
	printf("\e[%d;%dH", screen_line + 1, 0);
	int32_t i;
	for (i=0; i<n_lines; i++) {
		printf("\e[K\n");
	}
	return 0;
}



// NOTE: A clearscreen routine with simple prints is not being used, because in
// slower systems, the blank screen is noticeable.
int8_t GOPH_disp_print_block(GOPH_render_t* render, int32_t render_line,
                             int32_t screen_line, int32_t n_lines)
{
	int32_t i;

	// Go to screen origin
	printf("\e[%d;%dH", screen_line + 1, 0);
	
	// Simple Print
	for (i=render_line; i<render_line + n_lines; i++){
		// If the line starts with a tab, the print cursor will jump over the
		// initial characters that could be on the display. Therefore, a cleanup
		// must be made before printing.
		if (strchr(render->normal_list.list[i], 0x09)) {
			printf("\e[K");
		}
		printf("%s\e[K\n", render->normal_list.list[i]);
	}

	return 0;
}


int8_t GOPH_disp_print_changeBG(GOPH_render_t* render, int32_t render_pos,
                                int32_t screen_pos,	int32_t n_lines,
								int32_t showBG)
{
	int32_t screen_i;
	int32_t render_i;
	int32_t i;
	for (i=0; i<n_lines; i++) {
		screen_i = screen_pos + i;
		render_i = render_pos + i;
		
		// Move to line position
		printf("\e[%d;%dH", screen_i + 1, 0);

		if (showBG) {
			// Print with BG
			printf("%s", render->link_list.list[render_i]);
		} else {
			// Print normal content
			printf("%s", render->normal_list.list[render_i]);
		}
	}
	return 0;
}


int8_t GOPH_pager_printBG(GOPH_render_t* render, GOPH_pagerState_t* pagerState,
                          int32_t render_pos, int32_t screen_pos,
						  int32_t n_lines, int32_t showBG)
{
	int32_t screen_i;
	int32_t render_i;
	int32_t i;

	int32_t text_h = pagerState -> text_h;
	
	// Test lines are out of bounds
	if ((screen_pos >= text_h) || (screen_pos + n_lines <= 0)) {
		return 1;
	}
	
	// Fix iterator limits if needed
	int32_t i_min = screen_pos < 0 ? -screen_pos : 0;
	int32_t i_max = screen_pos + n_lines > text_h ? text_h - screen_pos : n_lines;


	for (i = i_min; i<i_max; i++) {
		screen_i = screen_pos + i;
		render_i = render_pos + i;
		
		// Move to line position
		printf("\e[%d;%dH", screen_i + 1, 0);

		if (showBG) {
			// Print with BG
			printf("%s", render->link_list.list[render_i]);
		} else {
			// Print normal content
			printf("%s", render->normal_list.list[render_i]);
		}
	}
	return 0;
}


int8_t GOPH_disp_print_menu (int32_t screen_w, int32_t screen_pos,
                             int32_t link_n)
{
	const char* menu_help = " h/l:back/enter  w/s:up/down  L:forward  "
		"r:reload  q:quit |";
	const int32_t menu_help_len = strlen(menu_help);

	int32_t menu_linkNum_len = 0;
	if (link_n == 0){
		menu_linkNum_len = 1;
	} else {
		//menu_linkNum_len = log10(abs(link_n)) + 1;
		//if (link_n < 0) {
		//	menu_linkNum_len += 1;
		//}
		menu_linkNum_len = GOPH_str_i32_len(link_n);
	}
	// 9 chars constantes + 1 char do blinker + n. chars do numero
	int32_t menu_link_len = 10 + menu_linkNum_len;

	// Move to line position
	printf("\e[%d;%dH", screen_pos + 1, 0);

	if (menu_help_len + menu_link_len <= screen_w) {
		printf("%s link:%i | \e[K", menu_help, link_n);
	} else {
		printf(" link:%i | \e[K", link_n);
	}
	return 0;
}


int8_t GOPH_pager_scroll(GOPH_render_t* render, GOPH_pagerState_t* pagerState,
                         int32_t move)
{
	int32_t render_pos = pagerState->render_pos;
	int32_t render_len = (int32_t)(render->normal_list.len);
	int32_t text_h     = pagerState->text_h;

	int32_t max_pos = render_len - text_h;
	int32_t new_pos = render_pos + move;

	// If the render size is less that the screen size, then no move can be done
	// Also prevents that a max_pos <= 0 could be accepted.
	if (render_len <= text_h) {
		return 0;
	}

	if (new_pos < 0) {
		new_pos = 0;
	} else if (new_pos > max_pos) {
		new_pos = max_pos;
	}
	
	pagerState->render_pos = new_pos;

	return 0;
}


// Calculates the render position in order to frame the new link
int8_t GOPH_pager_scrollLink(GOPH_render_t* render,
                             GOPH_pagerState_t* pagerState, int32_t move)
{
	// ENVIRONMENT VARIABLES
	int32_t n_lines = (int32_t)(render->normal_list.len);
	int32_t n_links = (int32_t)(render->nlinks);
	int32_t render_pos = pagerState->render_pos;
	int32_t link_n     = pagerState->link_n;
	int32_t text_h     = pagerState->text_h;

	int32_t render_top = render_pos;
	int32_t render_bot = render_top + text_h;
	
	int32_t new_link = link_n + move;
	int32_t new_link_top = 0;
	int32_t new_link_bot = 0;

	if (new_link < 0) {
		new_link = -1;
		new_link_top = 0;
		new_link_bot = 0;
	} else if (new_link >= n_links) {
		new_link = n_links;
		new_link_top = n_lines;
		new_link_bot = n_lines;
	} else {
		new_link_top = render->link_location[new_link];
		new_link_bot = new_link_top + render->link_nlines[new_link];
	}

	
	if (new_link_top < render_top) {
		render_pos = new_link_top;
	} else if (new_link_bot >= render_bot) {
		render_pos = new_link_bot - text_h;
		if (render_pos < 0)
			render_pos = 0;
	}
	
	// Write out the calculated value
	pagerState->render_pos = render_pos;
	pagerState->link_n     = new_link;
		
	return 0;
}



// Estou-me a borrifar para links.
// Só quero saber qual é a primeira linha
// Não quero dúvidas nem regras nem estados internos.
// Não quero output nenhum, a não ser erros.

// Vou dar as posição inicial no ecrã e a primeira linha do render e o número
// de linhas para fazer print e acabou-se. (e o link activo?)


// A Lógica do pager vai ser toda calculada aqui.
// Depois é só chamar as funções necessárias para formar a imagem.

//FIXME: pensar no flow do pager: links vs linhas normais, limites da página,
//      etc...
//FIXME: acho que vou ter três funções que vão manipular o estado do pager
//		- para os saltos de link
//		- para os saltos de 1 linha (ou n linhas)
//      - para o print final do estado do pager
// O ESTADO DO PAGER VAI SER O OBJECTO CENTRAL PARA METER ISTO A ANDAR


int8_t GOPH_disp_pager(GOPH_render_t* render, GOPH_pagerState_t* pagerState,
                       GOPH_pagerState_t* pagerState_old, uint8_t first_print)
{
	// INPUT READ
	int32_t text_h     = pagerState->text_h;
	int32_t render_pos = pagerState->render_pos;
	int32_t link_n     = pagerState->link_n;
	
	int32_t link_n_old = pagerState_old->link_n;

	int32_t nLinks    = render->nlinks;
	int32_t nLines    = render->normal_list.len;

	// ENVIRONMENT VARIABLES
	//FIXME: esta informação devia vir de fora
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	int32_t h = ws.ws_row;
	int32_t w = ws.ws_col;


	//FIXME talvez seja melhor calcular a posição estimada primeiro
	//FIXME estas condições estão todas a olho.
	// Prevent going above the limit
	
	int32_t link_pos_screen = -1;
	int32_t linkLoc    = -1;
	int32_t linkLen    = -1;

	if ((link_n >= 0) && (link_n < nLinks)) {
		linkLoc = render->link_location[link_n];
		linkLen = render->link_nlines[link_n];
		link_pos_screen = linkLoc - render_pos;
	}

	int32_t link_pos_screen_old = -1;
	int32_t linkLoc_old    = -1;
	int32_t linkLen_old    = -1;

	if ((link_n_old >= 0) && (link_n_old < nLinks)) {
		linkLoc_old = render->link_location[link_n_old];
		linkLen_old = render->link_nlines[link_n_old];
		link_pos_screen_old = linkLoc_old - render_pos;
	}


	// EMPTY LINES
	int32_t visibleLines = nLines - render_pos;
	// Clamp
	visibleLines = text_h > visibleLines ? visibleLines : text_h;
	int32_t emptyLines   = text_h - visibleLines;

	if (emptyLines > 0) {
		GOPH_disp_clear_block(visibleLines, emptyLines);
	}

	
	// GENERAL SCREEN MOVEMENT
	int32_t screen_move = pagerState->render_pos - pagerState_old->render_pos;
	int32_t remaining_lines = 0;

	// Por uma questão de compatibilidade, não uso escapes para mover o ecrã
	if ((screen_move > 0) && (screen_move < text_h) && 0) {
		
		printf("\e[1,%dr", text_h);
		printf("\e[%dS", screen_move);
		printf("\e[r");

		//printf("\e[%dS", screen_move);
		//for (int32_t j=0; j<screen_move; j++)
		//	printf("\n");
		remaining_lines = text_h - screen_move;
		GOPH_disp_print_block(render, render_pos + remaining_lines,
		                      remaining_lines, screen_move);
	}else if ((screen_move < 0) && (screen_move > -text_h) && 0) {
		printf("\e[%d;%dH", 1, 0);
		for (int32_t j=0; j<(-screen_move); j++)
			printf("\n");
		//printf("\e[%dT", -screen_move);
		GOPH_disp_print_block(render, render_pos, 0, -screen_move);
	}else if ((screen_move != 0) || first_print) {
		GOPH_disp_print_block(render, render_pos, 0, visibleLines);
	}


	// Turn off old link
	GOPH_pager_printBG(render, pagerState, linkLoc_old, link_pos_screen_old,
	                   linkLen_old, 0);

	// Turn on new link
	GOPH_pager_printBG(render, pagerState, linkLoc,     link_pos_screen,
	                   linkLen,     1);


	if ((pagerState_old->link_n != link_n) || first_print) {
		GOPH_draw_lineH(w, h-1, "▚");
		
		// This is drawn last, so that the blinker is placed at the bottom
		GOPH_disp_print_menu(w, h-1, link_n);
	}

	// Put cursor in final position
	printf("\e[%d;%dH", h, w);
	fflush(stdout);


	// Save old state
	pagerState_old->text_h     = text_h;
	pagerState_old->render_pos = render_pos;
	pagerState_old->link_n     = link_n;

	return 0;
}





// DISP LOGIC / MAIN LOOP

int32_t GOPH_page_show(GOPH_page_t* GOPHER_Page, GOPH_state_t* state)
{	
	// CONFIGURAÇÃO DO TERMINAL
	struct termios old_tio, new_tio;
	// get the terminal settings for stdin
	tcgetattr(STDIN_FILENO,&old_tio);
	// we want to keep the old setting to restore them a the end
	new_tio=old_tio;
	// disable canonical mode (buffered i/o) and local echo
	new_tio.c_lflag &=(~ICANON & ~ECHO);
	// set the new settings immediately
	tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
	
	// Return value
	// If >= 0, then is a line position
	// If <  0, then is an order (quit, back)
	int32_t cmd_out = CMD_NULL;

	// Input char
	uint8_t c;
	
	// RENDER
	// Estrutra inicial do render;
	GOPH_render_t render = GOPH_render_new();

	// Estrutura do estado do pager
	GOPH_pagerState_t pagerState     = GOPH_pagerState_new();
	// Estado prévio
	GOPH_pagerState_t pagerState_old = GOPH_pagerState_new();
	// Estado prévio


	//FIXME
	// Isto devia ser feito internamente numa função
	pagerState.text_h         = GOPH_get_screen_height() - 2;
	pagerState.render_pos     = state->screen_top;
	pagerState.link_n         = state->link_n;

	pagerState_old.text_h     = GOPH_get_screen_height() - 2;
	pagerState_old.render_pos = state->screen_top;
	pagerState_old.link_n     = state->link_n;

	//int32_t link_location = -1;

	int32_t screen_w = 0;
	int32_t screen_w_old = 0;

	uint8_t first_print = 0;

	// LOOP DE FUNCIONAMENTO
	while (1) {
		// Check if screen was changed.
		screen_w = GOPH_get_screen_width();
		first_print = 0;
		if (screen_w != screen_w_old) {
			GOPH_render_reset(&render);
			GOPH_page_render(GOPHER_Page, &render);
			screen_w_old = screen_w;
			first_print = 1;
		}
		
		GOPH_disp_pager(&render, &pagerState, &pagerState_old, first_print);

		// UPDATE OUTPUT VARIABLES
		//FIXME: ISTO É MUITO MAU (para além das variáveis de nomes diferentes)
		state->screen_top = pagerState.render_pos;
		state->link_n     = pagerState.link_n;

		c=getchar();

		//TODO A posição da próxima linha tem de ser calculada antes de pedir
		//     o print acima
		
		switch (c) {
			case ' ':
				//space
				GOPH_pager_scroll(&render, &pagerState,
				                  pagerState.text_h/2);
				break;;
			case '\x7f':
				//backspace
				GOPH_pager_scroll(&render, &pagerState,
				                  -pagerState.text_h/2);
				break;;
			case 's':
				GOPH_pager_scroll(&render, &pagerState,  1);
				break;;
			case 'w':
				GOPH_pager_scroll(&render, &pagerState, -1);
				break;;
			case 'j':
				GOPH_pager_scrollLink(&render, &pagerState,  1);
				break;;
			case 'k':
				GOPH_pager_scrollLink(&render, &pagerState, -1);
				break;;
		}
		
		// ARROWS
		if (c==0x1b) {
			getchar();
			switch(getchar()) {
				case 'A':
					GOPH_pager_scroll(&render, &pagerState, -1);
					break;;
				case 'B':
					GOPH_pager_scroll(&render, &pagerState, 1);
					break;;
			}
		}
		
		if ((c == 10) || (c == 'l')) {
			// Only valid links can break the cicle
			if ((pagerState.link_n >= 0) &&
			    (pagerState.link_n < (int32_t)render.nlinks)) {
				cmd_out = CMD_GET;
				break;
			}
		}
		
		//FIXME: Rever o número das ordens
		//       Talvez substituir por um enum de ordens, em vez de códigos
		// History navigation
		if (c == 'h') {
			// h: back
			cmd_out = CMD_NAV_BACK;
			break;
		} else if (c == 'L') {
			// L: forward
			// (modificador do comportamento habitual?)
			cmd_out = CMD_NAV_FORWARD;
			break;
		} else if (c == 'r') {
			// r: reload
			cmd_out = CMD_NAV_RELOAD;
			break;
		} else if (c == 'q') {
			// q: quit
			cmd_out = CMD_QUIT;
			break;
		}
	}
	
	// Memory management
	GOPH_render_free(&render);
	
	// restore the former settings
	tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);

	return cmd_out;
}
