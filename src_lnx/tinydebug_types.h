#ifndef _TINYTYPES_DEBUG_H_
#define _TINYTYPES_DEBUG_H_

// GOPHER Types debug library

#include "tinytypes.h"
#include "tinystate.h"
#include "tinydebug.h"

int8_t GOPH_debug_line(GOPH_line_t* line);

int8_t GOPH_debug_state(GOPH_state_t* state);

#endif
