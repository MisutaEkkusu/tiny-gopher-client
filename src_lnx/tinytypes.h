#ifndef _TINYTYPE_H_
#define _TINYTYPE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h> // struct sockaddr_in


// Line type
// The only allocation will be on *ptr and pointers *disp, *sel and *dom will
// point to locations insisde this allocation.
typedef struct GOPH_line{
	char *ptr;
	int32_t len;
	char *disp;
	char *sel;
	char *dom;
	char type;
	uint16_t port;
} GOPH_line_t;


int8_t GOPH_line_init(GOPH_line_t* line);

int8_t GOPH_line_free(GOPH_line_t* line);

int8_t GOPH_line_reset(GOPH_line_t* line);

size_t GOPH_line_size(GOPH_line_t* line);

GOPH_line_t GOPH_line_new();

int8_t GOPH_line_setParams(GOPH_line_t* line, char type, char* disp, char* sel,
                           char* dom, uint16_t port);

GOPH_line_t GOPH_line_clone(GOPH_line_t* line);

int8_t GOPH_line_equal(GOPH_line_t* line0, GOPH_line_t* line1);

int8_t GOPH_line_print(GOPH_line_t* line);

// Advanced functions
// Buffer parser into line
int8_t GOPH_line_parser(char* buffer, size_t buffer_len, GOPH_line_t *data);

// Convert 
int GOPH_line_toString(GOPH_line_t* line, char** string);

// Generate URL
// gopher://dom/t/sel
int GOPH_line_toUrl(GOPH_line_t* line, char* string, int32_t stringSize);

int8_t GOPH_line_urlToLine(GOPH_line_t* line, char* buff);

int8_t GOPH_line_isLink(GOPH_line_t* line);




typedef struct GOPH_page{	

	char type; //page type (directory/file)
	
	//TODO mudar este "nlines" para algo mais standard, como "lines_len"
	int32_t nlines; //number of directory page lines
	GOPH_line_t* lines; //directory page lines

	int32_t buffer_size;
	char* buffer; //reception buffer for non-directory/non-html links 

}GOPH_page_t;

int8_t GOPH_page_init(GOPH_page_t* page);

int8_t GOPH_page_free(GOPH_page_t* page);

int8_t GOPH_page_reset(GOPH_page_t* page);

GOPH_page_t GOPH_page_new();

GOPH_page_t GOPH_page_clone(GOPH_page_t* page);

size_t GOPH_page_size(GOPH_page_t* page);

int8_t GOPH_page_print(GOPH_page_t* page);

int8_t GOPH_page_writeToFile(GOPH_page_t* page, char* filename);



// GOPH_list_str
// List of strings.
typedef struct GOPH_list_str{
	char** list;
	int32_t len;
	int32_t cap;
} GOPH_list_str;

int8_t GOPH_list_str_init(GOPH_list_str *list);

int8_t GOPH_list_str_free(GOPH_list_str *list);

GOPH_list_str GOPH_list_str_new();

int8_t GOPH_list_str_reset(GOPH_list_str *list);

int8_t GOPH_list_str_push(GOPH_list_str *list, char* str, int32_t str_len);

int8_t GOPH_list_str_pop(GOPH_list_str *list);




typedef struct GOPH_usr{
	
	int32_t sIndx;  //current user-selected/highlighted index
	uint16_t sPort; //selected index net port (70)
	char* sDom; 	//selected index base URL
	char* sSel; 	//selected index selector
	char sType;		//selected index hyperlink type (FILE, DIR, BIN, etc)
	
	//socket stuff
	int sockid; //socket ID
	struct sockaddr_in struAddr; //address info
	
}GOPH_usr_t;

int8_t GOPH_usr_init(GOPH_usr_t *usr);

GOPH_usr_t GOPH_usr_new();

int8_t GOPH_usr_free(GOPH_usr_t* usr);

int8_t GOPH_usr_reset(GOPH_usr_t* usr);


// User input commands
typedef enum GOPH_cmd{
	CMD_NULL,
	CMD_QUIT,
	CMD_GET,
	CMD_NAV_BACK,
	CMD_NAV_FORWARD,
	CMD_NAV_RELOAD,
	CMD_SIZE,
	} GOPH_cmd_t;
	


#endif
