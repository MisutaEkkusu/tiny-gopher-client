#include "tinysocket.h"
#include "tinydebug.h"

void GOPH_SetupSockAddrIn(uint16_t port, int16_t family, struct sockaddr_in* struAddr){
	
	struAddr->sin_family = family;
	struAddr->sin_port = htons(port); 
	
}

uint8_t GOPH_ResolveURL(char* URL, struct sockaddr_in* struAddr){
	
	uint16_t h = 0;	
	struct hostent* remoteHost;
	
	remoteHost = gethostbyname(URL);
	
	if(remoteHost==NULL){
		fprintf(stderr,"ERROR: failed to get host IP\n");
		return 1;
	}

	if (remoteHost->h_addrtype == AF_INET)
	{
		while (remoteHost->h_addr_list[h] != 0) {
			struAddr->sin_addr.s_addr = *(uint32_t *) remoteHost->h_addr_list[h++];
			//printf("\tIP Address #%d: %s\n", h, inet_ntoa(struAddr->sin_addr));
			godmon("\tIP Address #%d: %s\n", h, inet_ntoa(struAddr->sin_addr));
		}
	}
	return 0; 
}

uint8_t GOPH_Connect(struct sockaddr_in struAddr, int* socket_id){
	
	*socket_id = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	if(*socket_id<0){
		fprintf(stderr,"ERROR: failed to create socket\n");
		return 0;
	}

	if(connect(*socket_id, (const struct sockaddr*)&struAddr,
	           sizeof(struAddr))<0){
		fprintf(stderr,"ERROR: Connection failed.\n");
		close(*socket_id);
		return 0;
	}
	return 1;
}


uint8_t GOPH_Connect_nonblock(struct sockaddr_in struAddr, int* socket_id){
	
	*socket_id = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	if(*socket_id<0){
		fprintf(stderr,"ERROR: failed to create socket\n");
		return 0;
	}

	// Set non block
	int flags = fcntl(*socket_id, F_GETFL);
	flags |= O_NONBLOCK;
	fcntl(*socket_id, F_SETFL, flags);
	
	godmon("[conn] Begin\n");
	struct timespec tv;
	tv.tv_sec  = 0;
	tv.tv_nsec = 200000000;

	int n_tries = 0;

	while (1) {

		n_tries++;
		
		//FIXME Esta abordagem é brute-force. Devia usar o select() para
		//      verificar o estado da ligação.
		errno = 0;
		int ret = connect(*socket_id, (const struct sockaddr*)&struAddr,
			   sizeof(struAddr));
		
		if (ret == 0) {
			godmon("[conn] Connection established.\n");
			godmon("[conn] n_tries: %d\n", n_tries);
			// Godmon separator
			godmon("\n");
			return 0;
		}

		//FIXME provavelmente parte destes erros nunca são alcançados
		switch (errno) {
			case EALREADY:
				godmon("[conn] EALREADY: A previous conn. attempt has not yet "
				       "been completed.\n");
				break;
			case EINPROGRESS:
				godmon("[conn] EINPROGRESS : The conn. cannot be completed "
				       "immediately\n");
				break;
			case ECONNREFUSED:
				godmon("[conn] ECONNREFUSED: Stream socket found no one "
				       "listening on the remote address.");
				return 1;
			case EISCONN:
				godmon("[conn] EISCONN: The socket is already connected.\n");
				return 0;
			default:
				godmon("[conn] %d : Unknow code.\n", errno);
		}
		
		nanosleep(&tv, NULL);
	}
	
	// FIXME Eu acho que este return está só a encher chouriços.
	return 0;
}


// Assuming buffer_size as the total bytes already downloaded
int8_t GOPH_GetFile_loop(int sockid, size_t* buffer_size, char**buffer)
{
	int32_t inputBytes = 0;
	char input_buffer[RECVBLOCKSZ];

	struct timespec dt_wait_dl;
	dt_wait_dl.tv_sec  = 0;
	dt_wait_dl.tv_nsec = 400000000;
	
	int32_t n_tries = 0;

	while (1) {
		n_tries++;

		errno = 0;
		inputBytes = recv(sockid, input_buffer, RECVBLOCKSZ, 0);

		if (!errno) {
			if (n_tries > 1)
				godmon("[download] (if >1) n_tries: %d\n", n_tries);
			break;
		}

		if (errno == EAGAIN) {
			godmon("[download] EAGAIN\n");
		}else{
			godmon("[download] %d : code not reconigzed\n", errno);
		}
		nanosleep(&dt_wait_dl, NULL);
	}

	if(inputBytes <= 0){
		// The download is over.
		// Nothing to update.
		return 1;
	}
	
	*buffer = realloc(*buffer, *buffer_size + inputBytes);
	if(*buffer == NULL){
		fprintf(stderr,"ERROR: file buffer realloc failed!\n");
		*buffer_size = 0;
		return 2;
	}
	memcpy((*buffer) + (*buffer_size), input_buffer, inputBytes* sizeof(char));
	*buffer_size += inputBytes;

	return 0;
}


//TODO Adptar esta função para usar a função loop
//     Esta função básicamente saca tudo de uma vez.
int8_t GOPH_GetFile_nonblock(int sockid, size_t* buffer_size, char** buffer)
{
	int32_t Msg_bytesRead = 0;
	int32_t totalBytesRead = 0, offset = 0;
	char Msg_recv[RECVBLOCKSZ];

	// Progress display
	int32_t totalBytes = 0;
	// Penso que 32 chars conseguem conter os digitos de um float e as unidades
	//char    msg[32];

	struct timespec dt_wait_dl;
	dt_wait_dl.tv_sec  = 0;
	dt_wait_dl.tv_nsec = 400000000;
	
	int n_tries = 0;
	do{
		n_tries = 0;
		while (1) {
			n_tries++;

			errno = 0;
			Msg_bytesRead = recv(sockid,Msg_recv,RECVBLOCKSZ,0);

			if (!errno) {
				if (n_tries > 1)
					godmon("[download] (if >1) n_tries: %d\n", n_tries);
				break;
			}

			if (errno == EAGAIN) {
				godmon("[download] EAGAIN\n");
			}else{
				godmon("[download] %d : code not reconigzed\n", errno);
			}
			nanosleep(&dt_wait_dl, NULL);
		}

		// Progress display
		totalBytes += Msg_bytesRead;
		
		if(Msg_bytesRead > 0){
			totalBytesRead += Msg_bytesRead;
			*buffer = realloc(*buffer,totalBytesRead);
			if(*buffer == NULL){
				totalBytesRead = 0; 
				fprintf(stderr,"ERROR: file buffer realloc failed!\n");
				break;
			}
			
			memcpy((*buffer)+offset, Msg_recv, Msg_bytesRead * sizeof(char));
			offset += Msg_bytesRead;
		}

	}while(Msg_bytesRead>0);
	
	// Godmon separator
	godmon("\n");

	*buffer_size = totalBytesRead;
	return 0;
}


uint8_t GOPH_Close(int sockid){
	close(sockid);
	return 1;
}
