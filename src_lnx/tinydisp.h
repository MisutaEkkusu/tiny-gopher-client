#ifndef _TINYDISP_H_
#define _TINYDISP_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
//#include <math.h>

#include "tinydraw.h"
#include "tinytypes.h"
#include "tinystate.h"
#include "tinystring.h"

// Structure holding the information for displaying.
typedef struct GOPH_render_t{
	GOPH_list_str normal_list;
	GOPH_list_str link_list;
	// Number of links
	int32_t nlinks;
	// Link location in page_t
	int32_t* link_location_page;
	// Link location in render lines
	int32_t* link_location;
	// Number of render lines for each link
	int32_t* link_nlines;
} GOPH_render_t;

int8_t GOPH_render_init(GOPH_render_t* render);

int8_t GOPH_render_free(GOPH_render_t* render);

GOPH_render_t GOPH_render_new();

int8_t GOPH_render_reset(GOPH_render_t* render);

int8_t GOPH_render_dump(GOPH_render_t* render);

int8_t GOPH_render_debug_dump(GOPH_render_t* render);


// Drawing functions
int8_t GOPH_draw_text(int32_t row, int32_t col, char* msg);

int8_t GOPH_draw_lineH(int32_t termW, int32_t row, char* c);

int8_t GOPH_draw_stripH(int32_t width, int32_t col, int32_t row, char* c);

int8_t GOPH_draw_textLineWithBG(int32_t row, int32_t col, char* msg,
                                char* bgChar);





// GOPHER RENDER Engine
int32_t GOPH_get_screen_width();

int8_t GOPH_line_render(char code, uint8_t has_bg, uint8_t has_prefix,
                        char* line, char* buffer, int32_t line_len);

// Render dir files
int8_t GOPH_dir_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render);

// Render text files
int8_t GOPH_txt_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render);

// Render dispatcher
int8_t GOPH_page_render(GOPH_page_t* GOPHER_Page, GOPH_render_t *render);

//int8_t GOPH_print_screen(GOPH_render_t* render, int32_t* screen_top_in,
//                         int32_t* link_n_in, uint8_t* link_in_screen_in,
//                         int32_t move_direction);



// NEW RENDER ENGINE
typedef struct GOPH_pagerState {
	int32_t render_pos;
	int32_t link_n;
	int32_t text_h;
} GOPH_pagerState_t;

int8_t GOPH_pagerState_init(GOPH_pagerState_t* state);

GOPH_pagerState_t GOPH_pagerState_new();

int8_t GOPH_disp_clear_block(int32_t screen_line, int32_t n_lines);

int8_t GOPH_disp_print_block(GOPH_render_t* render, int32_t render_line,
                             int32_t screen_line, int32_t n_lines);

int8_t GOPH_disp_print_changeBG(GOPH_render_t* render, int32_t render_pos,
                                int32_t screen_pos,	int32_t n_lines,
								int32_t showBG);

int8_t GOPH_disp_print_menu (int32_t screen_w, int32_t screen_pos,
                             int32_t link_n);

int8_t GOPH_pager_scroll(GOPH_render_t* render, GOPH_pagerState_t* pagerState,
	int32_t move);

int8_t GOPH_pager_scrollLink(GOPH_render_t* render,
	GOPH_pagerState_t* pagerState, int32_t move);

int8_t GOPH_disp_pager(GOPH_render_t* render, GOPH_pagerState_t* pagerState,
	GOPH_pagerState_t* pagerState_old, uint8_t first_print);



// ENTRY FUNCTION

int32_t GOPH_page_show(GOPH_page_t* GOPHER_Page, GOPH_state_t* state);


#endif
