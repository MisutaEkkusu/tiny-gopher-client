#ifndef _TINYDRAW_H_
#define _TINYDRAW_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>

// Drawing functions
int8_t GOPH_draw_text(int32_t row, int32_t col, char* msg);

int8_t GOPH_draw_lineH(int32_t termW, int32_t row, char* c);

int8_t GOPH_draw_stripH(int32_t width, int32_t col, int32_t row, char* c);

int8_t GOPH_draw_textLineWithBG(int32_t row, int32_t col, char* msg,
                                char* bgChar);

#endif 
