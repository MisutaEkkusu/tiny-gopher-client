#ifndef _TINYHIST_H_
#define _TINYHIST_H_

#include <stdlib.h>
#include <stdint.h>
#include "tinytypes.h"
#include "tinystate.h"
#include "tinydebug.h"
#include "tinydebug_types.h"


typedef struct GOPH_hist_t {
	int32_t      len;
	int32_t      cap;
	int32_t      pos;
	GOPH_state_t* ptr;
} GOPH_hist_t;


// HIST STRUCTURE

int8_t GOPH_hist_init(GOPH_hist_t* hist);

int8_t GOPH_hist_free(GOPH_hist_t* hist);

GOPH_hist_t GOPH_hist_new();

int8_t GOPH_hist_reset(GOPH_hist_t* hist);

int8_t GOPH_hist_push(GOPH_hist_t* vec, GOPH_state_t* state);

int8_t GOPH_hist_pop(GOPH_hist_t* vec, GOPH_state_t* state);

int8_t GOPH_hist_debug(GOPH_hist_t* hist);



// HIST MANIPULATION
int8_t GOPH_hist_getStateFromPage(GOPH_page_t*  page, GOPH_state_t* state_in,
                                  GOPH_state_t* state_out);

int8_t GOPH_hist_updateState(GOPH_hist_t* hist, ssize_t pos,
                             GOPH_state_t* state);

int8_t GOPH_hist_addTopState(GOPH_hist_t* hist, GOPH_state_t* state);

int8_t GOPH_hist_stepForw(GOPH_hist_t* hist, GOPH_state_t* state_io);

int8_t GOPH_hist_stepBack(GOPH_hist_t* hist, GOPH_state_t* state_io);

int8_t GOPH_hist_removeAllAbovePos(GOPH_hist_t* hist);


#endif
