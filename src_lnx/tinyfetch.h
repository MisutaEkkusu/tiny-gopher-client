#ifndef _TINYFETCH_H_
#define _TINYFETCH_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "tinysocket.h"
#include "tinytypes.h"
#include "tinystring.h"
#include "tinydraw.h"
#include "tinyparse.h"
#include "tinydebug.h"


#define RECVBLOCKSZ 256

//int8_t GOPH_dirParse(size_t buffer_size, char* buffer, GOPH_page_t* page);

//int8_t GOPH_GetDirPage(int sockid, GOPH_page_t* page);

//int8_t GOPH_GetFile(int sockid, GOPH_page_t* page);
int8_t GOPH_GetFile(int sockid, size_t* buffer_size, char** buffer);

int8_t GOPH_fetch_makeErrorPage(GOPH_page_t* page, char* message);

int8_t GOPH_fetch_setupConnection(GOPH_usr_t* user);

int8_t GOPH_fetch_page(GOPH_usr_t* user, GOPH_page_t* GOPHER_Page);



// USER FUNCTIONS
int8_t GOPHER_fetch_setUserSelect(GOPH_usr_t* user, GOPH_line_t* line_next);

#endif
