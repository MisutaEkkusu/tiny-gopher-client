#ifndef _TINYSOCKET_H_
#define _TINYSOCKET_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <arpa/inet.h> //inet_pton()
#include <netdb.h> //gethostbyname()

#include <fcntl.h>  //non blocking sockets and flags
#include <string.h> //memcpy()
#include <time.h> 
#include <errno.h>

#define RECVBLOCKSZ 256

void GOPH_SetupSockAddrIn(uint16_t port, int16_t family, struct sockaddr_in* struAddr);
uint8_t GOPH_ResolveURL(char* URL, struct sockaddr_in* struAddr);
uint8_t GOPH_Connect(struct sockaddr_in struAddr, int* socket_id);

uint8_t GOPH_Connect_nonblock(struct sockaddr_in struAddr, int* socket_id);
int8_t GOPH_GetFile_loop(int sockid, size_t* buffer_size, char** buffer);
int8_t GOPH_GetFile_nonblock(int sockid, size_t* buffer_size, char** buffer);

uint8_t GOPH_Close(int sockid);
#endif
