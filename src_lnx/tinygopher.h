#ifndef _TINYGOPHER_H_
#define _TINYGOPHER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>

#include "tinydisp.h"
#include "tinytypes.h"
#include "tinyhist.h"
#include "tinyfetch.h"
#include "tinycache.h"
#include "tinyconfig.h"
#include "tinystring.h"
#include "tinydebug.h"
#include "tinydebug_types.h"

//TERMIOS
#include <termios.h>


void tinygopher_init();

void tinygopher_finish();

#endif
