#ifndef _TINYPAGEMAKER_H_
#define _TINYPAGEMAKER_H_

/*
	PAGEMAKER

	Creates custom pages
*/


#include "tinytypes.h"

int8_t GOPH_pagemaker_text(GOPH_page_t* page, char* text);

#endif
