#ifndef _TINYRENDER_H_
#define _TINYRENDER_H_

#include <stdint.h>
#include <stdio.h>
#include <stdint.h>

#include "tinytypes.h"

int8_t GOPH_render_pageInfo(GOPH_page_t* pout, GOPH_line_t* line);

#endif
