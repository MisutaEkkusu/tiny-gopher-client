#ifndef _TINYDEBUG_H_
#define _TINYDEBUG_H_

#include<stdlib.h>
#include<stdio.h>
#include<sys/stat.h>
#include<unistd.h>
#include<signal.h>
//#include<fcntl.h>
//#include<sys/types.h>

//Variable number of arguments
#include<stdarg.h>


int godmon_init();

int godmon_print(char* tag, char* msg_fmt, va_list args);

int godmon(char* msg_fmt, ...);

int godmontag(char* tag, char* msg_fmt, ...);

int godmon_monitor();

// ctrl+c handler
void godmon_monitor_exit();

#endif
