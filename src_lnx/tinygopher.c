#include "tinygopher.h"

const uint8_t CACHE_ON = 1;

//FIXME: COMO DESLIGAR O HISTÓRICO?
const uint8_t HIST_ON = 1;


void tinygopher_init() {
	// ALTERNATE SCREEN BUFFER - ENTER
	// Output de $ tput smcup | hd
	printf("\e[?1049h\e[22;0;0t");
}


//FIXME: Falta fazer reset ao modo de input
void tinygopher_finish()
{	
	// CONFIGURAÇÃO DO TERMINAL
	struct termios new_tio;
	// get the terminal settings for stdin
	tcgetattr(STDIN_FILENO, &new_tio);
	// enable canonical mode (buffered i/o) and local echo
	new_tio.c_lflag |= (ICANON | ECHO);
	// set the new settings immediately
	tcsetattr(STDIN_FILENO, TCSANOW, &new_tio);

	// ALTERNATE SCREEN BUFFER - EXIT
	// Output de $ tput rmcup | hd
	printf("\e[?1049l\e[23;0;0t");
	exit(0);
}


int main(int argc, char* argv[])
{
	
	// FIXME: Tenho de divir a inicialização em etapas.
	// ex:
	// - (onde meter as variáveis iniciais?)
	// - sinais
	// - criação das estruturas (com valores pré-definidos)
	// - inicialização da estrutura das configurações (com valores pré-definidos)
	// - leitura (e validação) das configurações
	// - escrita das configurações

	// SIGNAL
	// ctrl+c handling
	struct sigaction act = {0};
	act.sa_handler = tinygopher_finish;
	sigaction(SIGINT, &act, NULL);


	// READ CONFIGURATIONS FROM CONFIG FILE
	GOPH_config_t config = GOPH_config_new();
	int8_t ret = GOPH_config_readFile(&config);
	if (ret)
		godmon("main", "configuration file not found\n");

	// READ CONFIGURATIONS FROM ARGV
	GOPH_config_readArgv(&config, argc, argv);

	// SCREEN INITIALIZATION
	tinygopher_init();
	
	
	// Gopher Debuging Monitor initialization
	godmon_init();
	godmon("########################################"
	       "########################################\n");
	godmon("WELCOME TO PAIN\n");
	
	// History initialization
	GOPH_hist_t hist = GOPH_hist_new();
	
	// Gopher Page structure
	GOPH_page_t GOPHER_Page = GOPH_page_new();
	
	// Primeira linha do histórico (página inicial)
	GOPH_line_t line_next = GOPH_line_new();
	// TODO: Talvez a verificação do url devesse ser feita nas respectivas
	//       funções que lêm o url.
	if (GOPH_line_urlToLine(&line_next, config.homepage)) {
		tinygopher_finish();
		exit(2);
	}

	// User request structure
	GOPH_usr_t user = GOPH_usr_new();

	// Inital screen position
	int32_t screen_top = 0;
	int32_t link_n = -1;


	//ESTADO NOVO
	//FIXME: Talvez possa meter as funções a comer o gopher_state em vez destas
	// variáveis. Por outro lado, se quiser desligar o histórico, não o vou
	// fazer.
	GOPH_state_t state;
	state.page_line    = GOPH_line_clone(&line_next);
	state.screen_top   = screen_top;
	state.link_n       = link_n;
	GOPH_state_t state_new = GOPH_state_new();

	if (HIST_ON) {
		GOPH_hist_addTopState(&hist, &state);
	}

	int32_t userCmd = CMD_NULL;


	GOPH_cache_t cache = GOPH_cache_new();
	int8_t cacheHasPage = 0;


	while(1) {
		godmon("\n\n");
		godmon("----------------------------------------"
		       "----------------------------------------\n");

		//FIXME Reorganizar a lógica da cache. Simplificar quase ao nível de um
		//      mecanismo sem poder de decisão

		
		// CACHE :: userCmd ORDER
		// Remove page from cache to reload it.
		if (CACHE_ON && userCmd == CMD_NAV_RELOAD) {
			godmontag("main", "CACHE before remove page\n");
			GOPH_cache_debug(&cache);
			GOPH_cache_removePage(&cache, &line_next);
			cacheHasPage = 0;
		}
		
		
		// PAGE RETRIEVAL
		
		godmon("CACHE\n");
		GOPH_cache_debug(&cache);
		
		// 1) Check cache
		if (CACHE_ON) {
			cacheHasPage = GOPH_cache_hasPage(&cache, &line_next);
			godmontag("main", "cache_hasPage = %d\n", cacheHasPage);
		}
		
		// 2) Try loading page from cache, if not, download it.
		if (cacheHasPage) {
			godmontag("main", "loading page from cache\n");
			GOPHER_Page = GOPH_cache_loadPage(&cache, &line_next);
		} else {
			godmontag("main", "loading page from internet\n");
			// Setup parameters for download
			GOPHER_fetch_setUserSelect(&user, &line_next);
			// Download
			if(GOPH_fetch_page(&user, &GOPHER_Page)){
				godmontag("main", "Cannot download page.\n");
			}
			// Close connection
			GOPH_Close(user.sockid);
		}
		
		// 3) Try to save the page to cache
		if (CACHE_ON && (!cacheHasPage)) {
			godmontag("main", "save page to cache\n");
			cacheHasPage = !GOPH_cache_savePage(&cache, &line_next,
			                                    &GOPHER_Page);
		}
		godmontag("main", "cacheHasPage: %d\n", cacheHasPage);
		godmon("\n");
		
		godmontag("main", "PAGE\n");
		godmontag("main", "type:%c nlines %i\n", GOPHER_Page.type,
		          GOPHER_Page.nlines);
		godmon("\n");
		


		godmon("HIST 0\n");
		GOPH_hist_debug(&hist);
		godmon("\n");
		godmon("STATE\n");
		GOPH_debug_state(&state);
		godmon("\n");
		
		
		// USER INPUT
		userCmd = GOPH_page_show(&GOPHER_Page, &state);

		if (userCmd == CMD_GET) {
			GOPH_hist_getStateFromPage(&GOPHER_Page, &state, &state_new);
		}
		
		
		godmontag("main", "userCmd: %i\n", userCmd);
		godmon("\n");
		godmon("STATE AFTER CMD\n");
		GOPH_debug_state(&state);
		godmon("\n");

		godmontag("main", "state_new\n");
		GOPH_debug_state(&state_new);



		//FIXME Dá vontade de meter este switch dentro de um "if (HIST_ON)", mas
		//      para poder fazer isso, terei de extrair o estado à página antes
		//      de chegar a este bloco.
		//FIXME Deverei duplicar o estado ou apenas modificá-lo, como estou a
		//      fazer?

		switch (userCmd) {
			case CMD_NAV_BACK:
				GOPH_hist_updateState(&hist, hist.pos, &state);
				GOPH_hist_stepBack(&hist, &state);
				break;;
			case CMD_NAV_FORWARD:
				GOPH_hist_updateState(&hist, hist.pos, &state);
				GOPH_hist_stepForw(&hist, &state);
				break;;
			case CMD_GET:
				GOPH_hist_updateState(&hist, hist.pos, &state);
				GOPH_hist_removeAllAbovePos(&hist);
				//GOPH_hist_getStateFromPage(&GOPHER_Page, &state);
				//GOPH_hist_addTopState(&hist, &state);

				// new version
				//FIXME Isto está feito!!!
				GOPH_hist_addTopState(&hist, &state_new);
				GOPH_state_free(&state);
				state = GOPH_state_clone(&state_new);
				GOPH_state_reset(&state_new);
				break;;
		}
		

		/*
		//FIXME: Caso a ordem seja reload, só é necessário remover a página
		// antiga da cache. O line_next será o actual.
		
		if (HIST_ON) {
			// Update display parameters of current state
			GOPH_hist_updateState(&hist, hist.pos, &state);
			if (userCmd == CMD_NAV_BACK) {
				GOPH_hist_stepBack(&hist, &state);
			} else if (userCmd == CMD_NAV_FORWARD) {
				GOPH_hist_stepForw(&hist, &state);
			} else if (userCmd == CMD_GET) {
				GOPH_hist_removeAllAbovePos(&hist);
			}
		}
		
		
		// NEXT LINK
		
		// FIXME isto está confuso
		// Isto tem de estar aqui, porque se estivesse dentro do bloco do
		// histórico e se o histórico estivesse desactivado, então não era
		// possível carregar o próximo link.
		if (userCmd == CMD_GET) {
			//NOTA: o &state está a ser usado como i/o
			GOPH_hist_getStateFromPage(&GOPHER_Page, &state);
			if (HIST_ON) {
				GOPH_hist_addTopState(&hist, &state);
			}
		}
		*/

		//FIXME HACK
		//FIXME Tendo um tipo state, para que é que eu preciso deste line_next?
		//      Se quiser memorizar o estado antigo, posso simplesmente duplicar
		//      o estado.
		//Terei de me livrar do line next???
		GOPH_line_free(&line_next);
		line_next  = GOPH_line_clone(&(state.page_line));
		
		
		// DEBUG OUTPUT
		godmon("\n");
		godmon("LINE_NEXT:\n");
		GOPH_debug_line(&line_next);
		godmon("\n");
		
		godmon("HIST 1\n");
		GOPH_hist_debug(&hist);
		godmon("\n");
		
		godmon("\n");
		
		
		// CLEANUP
		/*
		if (CACHE_ON) {
			// If the page cannot be saved in cache, it must be destroyed.
			if (!cacheHasPage) {
				GOPH_page_free(&GOPHER_Page);
			}
			GOPHER_Page = GOPH_page_new();
		} else {
			GOPH_page_reset(&GOPHER_Page);
		}
		*/

		GOPH_page_reset(&GOPHER_Page);
		
		
		// EXIT POINT
		if (userCmd == CMD_QUIT)
			break;
		
	}
	
	// Memory management
	GOPH_state_free(&state);
	GOPH_usr_free(&user);
	GOPH_line_free(&line_next);
	GOPH_hist_free(&hist);
	GOPH_cache_free(&cache);
	GOPH_config_free(&config);
	
	// ALTERNATE SCREEN BUFFER - EXIT
	// Output de $ tput rmcup | hd
	printf("\e[?1049l\e[23;0;0t");
	
	return EXIT_SUCCESS;
}
