#include "tinytypes.h"

//////// LINE ////////

int8_t GOPH_line_init(GOPH_line_t* line)
{
	line->ptr  = NULL;
	line->len  = 0;
	line->disp = NULL;
	line->sel  = NULL;
	line->dom  = NULL;
	line->type = 0;
	line->port = 0;
	return 0;
}


int8_t GOPH_line_free(GOPH_line_t* line)
{
	free(line->ptr);
	return 0;
}


int8_t GOPH_line_reset(GOPH_line_t* line)
{
	GOPH_line_free(line);
	GOPH_line_init(line);
	return 0;
}


// This is a lower bound for line size.
// The compiler or operating system can add padding, increasing memory size.
size_t GOPH_line_size(GOPH_line_t* line)
{
	return line->len + sizeof(GOPH_line_t);
}


GOPH_line_t GOPH_line_new()
{
	GOPH_line_t line;
	GOPH_line_init(&line);
	return line;
}


int8_t GOPH_line_setParams(GOPH_line_t* line, char type, char* disp, char* sel,
                           char* dom, uint16_t port)
{
	GOPH_line_free(line);

	// Extra byte for \0 char.
	size_t disp_len = strlen(disp) + 1;
	size_t sel_len  = strlen(sel)  + 1;
	size_t dom_len  = strlen(dom)  + 1;
	size_t len = disp_len + sel_len + dom_len;

	line->ptr = calloc(len, 1);
	line->len = len;

	line->disp = line->ptr;
	line->sel  = line->disp + disp_len;
	line->dom  = line->sel  + sel_len;
	
	strcpy(line->disp, disp);
	strcpy(line->sel , sel);
	strcpy(line->dom , dom);
	
	line->type = type;
	line->port = port;
	
	return 0;
}


GOPH_line_t GOPH_line_clone(GOPH_line_t* line)
{
	GOPH_line_t clone;

	clone.ptr = malloc(line->len);
	memcpy(clone.ptr, line->ptr, line->len);
	clone.len  = line->len ;

	// Extra byte for \0.
	clone.disp = clone.ptr;
	clone.sel  = clone.disp + strlen(clone.disp) + 1;
	clone.dom  = clone.sel  + strlen(clone.sel ) + 1;
	clone.type = line->type;
	clone.port = line->port;

	return clone;
}


int8_t GOPH_line_equal(GOPH_line_t* line0, GOPH_line_t* line1)
{
	if (line0->type != line1->type ||
	    line0->len  != line1->len  ||
		line0->port != line1->port) {
		return 0;
	}

	if (strcmp(line0->disp, line1->disp) ||
	    strcmp(line0->sel , line1->sel ) ||
		strcmp(line0->dom , line1->dom )) {
		return 0;
	}
	return 1;
}


int8_t GOPH_line_print(GOPH_line_t* line)
{
	printf("gopher://%s/%c%s\n", line->dom, line->type, line->sel);
	printf("type: %c\n", line->type);
	printf("disp: %s\n", line->disp);
	printf("sel : %s\n", line->sel);
	printf("dom : %s\n", line->dom);
	printf("port: %u\n", line->port);

	return 0;
}


// Advanced functions
// I'm assuming that buffer is a null terminated string with size buffer_len.
int8_t GOPH_line_parser(char* buffer, size_t buffer_len, GOPH_line_t *data)
{
	// Small line test
	if (buffer_len < 2)
		return 1;

	// Empty line test
	if (buffer_len == 2 && buffer[0] == '\r' && buffer[1] == '\n')
		return 2;

	char type = buffer[0];
	
	// End of directory test
	if (type == '.')
		return 3;
	
	// Field delimiters
	size_t fields[4];
	fields[0] = 1;

	size_t i = 0;
	size_t c = 0;

	// Number of fields test
	for (i=1; i<buffer_len; i++) {
		if (!buffer[i]) {
			break;
		}
		c += (buffer[i] == '\t');
	}
	if (c != 3)
		return 4;

	// Do not modify the input buffer.
	c = 1;
	for(i=1; i<buffer_len; i++){
		if (!buffer[i]) {
			break;
		}
		if (buffer[i] == '\t') {
			fields[c] = i + 1;
			c++;
		}
		if (c==4) {
			break;
		}
	}

	// String lengths.
	size_t disp_len = fields[1] - fields[0];
	size_t sel_len  = fields[2] - fields[1];
	size_t dom_len  = fields[3] - fields[2];
	size_t len = fields[3] - fields[0];
	
	// Memory allocation and copy.
	data->ptr = malloc(len);
	memcpy(data->ptr, buffer + 1, len);
	data->len = len;

	// Set string pointers.
	data->disp = data->ptr;
	data->sel  = data->disp + disp_len;
	data->dom  = data->sel  + sel_len;

	// Set \0 chars.
	*(data->disp + disp_len - 1) = 0;
	*(data->sel  + sel_len  - 1) = 0;
	*(data->dom  + dom_len  - 1) = 0;

	// Copy of the numerical fields.
	data->type = type;
	data->port = atoi(buffer + fields[3]);

	return 0;
}


// Converts a line_t to a string.
// It will alocate the right size and output a pointer to the allocation.
//FIXME: O buffer não devia ser alocado aqui, mas exteriormente.
//FIXME: Talvez o compromisso seja indicar o tamanho do buffer de entrada e caso
// não tenha espaço, aumenta-o.
int GOPH_line_toString(GOPH_line_t* line, char** string)
{
	// Validate line
	if ((line->ptr != line->disp) || (line->disp >= line->sel) ||
	    (line->sel >= line->dom)) {
		return -1;
	}

	// Calculation of the number of chars for the port string.
	size_t  portSize = 0;
	int32_t aux      = 1;
	do {
		portSize++;
		aux *= 10;
	} while (aux <= line->port);

	// The string must reserve space for initial character, the three string
	// fields, the port, 2 end characters and the null character.
	size_t strSize = line->len + portSize + 4;

	char* strOut = malloc(strSize);

	// Type
	strOut[0] = line->type;
	
	// Disp, Sel and Dom.
	memcpy(strOut + 1, line->ptr, line->len);
	// Fix tabs (index inside new string)
	size_t disp_last = (line->sel) - (line->ptr);
	size_t sel_last  = (line->dom) - (line->ptr);
	size_t dom_last  = (line->len);
	strOut[disp_last] = '\t';
	strOut[sel_last]  = '\t';
	strOut[dom_last]  = '\t';
	
	// Port and end characters
	sprintf(strOut + dom_last + 1, "%u\r\n", line->port);

	*string = strOut;

	return strSize;
}


int GOPH_line_lineToUrl(GOPH_line_t* line, char* string, size_t stringSize)
{
	int nChar = snprintf(string, stringSize, "gopher://%s/%c%s", line->dom,
	                     line->type, line->sel);
	return nChar;
}


// Using the url itself as disp
int8_t GOPH_line_urlToLine(GOPH_line_t* line, char* buff)
{
	int32_t i,j;

	const char* prefix = "gopher://";
	const size_t prefix_len = strlen(prefix);
	
	int32_t addr_beg;
	int32_t addr_end;

	int32_t pos[3] = {-1, -1, -1};
	int32_t len[3] = { 0,  0,  0};

	int32_t buff_len = strlen(buff);
	

	// Find beginning
	for (i=0; i<buff_len; i++) {
		if (buff[i] != ' ' && buff[i] != '\t') {
			break;
		}
	}
	addr_beg = i;

	// It can start by the prefix
	if (strncmp(buff + i, prefix, prefix_len)) {
		return 1;
	}

	// FIXME: Find the rules of URI
	// If not, the first char must not be a '/' all the chars must not be ':' 


	pos[0] = addr_beg + prefix_len;

	// Find remaining separators
	j=0;
	for (i=pos[0]; i<buff_len; i++) {
		if (buff[i] == '/') {
			len[j] = i - pos[j];
			
			if (j == 2) {
				break;
			}

			pos[j+1] = i + 1;
			j++;
		}
	}

	// Agora vou determinar o tamanho do último campo
	for (i=pos[j]; i<buff_len; i++) {
		if (buff[i] == ' '  || buff[i] == '\t' ||
		    buff[i] == '\r' || buff[i] == '\n') {
			break;
		}
		if (j<2 && buff[i] == '/') {
			break;
		}
	}
	addr_end = i;
	len[j] = addr_end - pos[j];
		
	
	if (len[0] == 0) {
		return 1;
	}
	
	// disp_len = (len do endereço) + (\0)
	// sel_len  = (/) + (len sel) + (\0)
	// dom_len  = (len dom) + (\0)
	int32_t addr_len = addr_end - addr_beg;
	int32_t disp_len = addr_len + 1;
	int32_t sel_len  = 1 + len[2] + 1;
	int32_t dom_len  = len[0] + 1;


	int32_t sel_pos = disp_len;
	int32_t dom_pos = sel_pos + sel_len;
	
	// Contabilidade:
	// (len do endereço) + (\0) + (/) + (len sel) + (\0) + (len dom) + (\0)
	int32_t buff_out_len = disp_len + sel_len + dom_len;

	char* buff_out = malloc(buff_out_len);
	line->ptr = buff_out;
	line->len = buff_out_len;
	
	// disp
	strncpy(buff_out, buff + addr_beg, addr_len);
	line->disp = buff_out;
	buff_out[disp_len - 1] = '\0';

	// sel
	buff_out[sel_pos] = '/';
	strncpy(buff_out + sel_pos + 1, buff + pos[2], len[2]);
	line->sel = buff_out + sel_pos;
	buff_out[sel_pos + sel_len - 1] = '\0';

	// dom
	strncpy(buff_out + dom_pos, buff + pos[0], len[0]);
	line->dom = buff_out + dom_pos;
	buff_out[dom_pos + dom_len - 1] = '\0';
	
	// type
	if (len[1] > 0) {
		line->type = buff[pos[1]];
	} else {
		line->type = '1';
	}
	
	//port
	line->port = 70;
	
	return 0;
}


// Check if the line is a link
int8_t GOPH_line_isLink(GOPH_line_t* line)
{
	switch(line->type) {
		case '0':
			return 1;;
		case '1':
			return 1;;
		case 'h':
			return 1;;
		default:
			return 0;;
	}
}




//////// PAGE ////////

int8_t GOPH_page_init(GOPH_page_t* page)
{
	page->type        = 0;
	page->nlines      = 0;
	page->lines       = NULL;
	page->buffer_size = 0;
	page->buffer      = NULL;
	return 0;
}

int8_t GOPH_page_free(GOPH_page_t* page)
{
	int32_t i;
	if (page->type == '1') {
		for (i=0; i<page->nlines; i++){
			GOPH_line_free(&(page->lines)[i]);
		}
		free(page->lines);
	} else {
		free(page->buffer);
	}
	return 0;
}

int8_t GOPH_page_reset(GOPH_page_t* page)
{
	GOPH_page_free(page);
	GOPH_page_init(page);
	return 0;
}

GOPH_page_t GOPH_page_new()
{
	GOPH_page_t page;
	GOPH_page_init(&page);
	return page;
}


GOPH_page_t GOPH_page_clone(GOPH_page_t* page)
{
	GOPH_page_t clone;
	
	// Scalars
	clone.type   = page->type;
	clone.nlines = page->nlines;
	clone.buffer_size = page->buffer_size;

	// Lines
	if (page->lines == NULL) {
		clone.lines = NULL;
	} else {
		clone.lines = malloc(page->nlines * sizeof(GOPH_line_t));
	}

	int32_t i;
	for (i=0; i<clone.nlines; i++) {
		clone.lines[i] = GOPH_line_clone(&(page->lines)[i]);
	}

	// Buffer
	if (page->buffer == NULL) {
		clone.buffer = NULL;
	} else {
		clone.buffer = malloc(page->buffer_size);
		memcpy(clone.buffer, page->buffer, page->buffer_size);
	}
	
	return clone;
}


// This is a lower bound for page size.
// The compiler or operating system can add padding, increasing memory size.
size_t GOPH_page_size(GOPH_page_t* page)
{
	size_t page_size   = sizeof(GOPH_page_t);
	size_t lines_size  = 0;
	size_t buffer_size = page->buffer_size;

	int32_t i;
	for (i=0; i<page->nlines; i++) {
		lines_size += GOPH_line_size(page->lines + i);
	}

	return page_size + lines_size + buffer_size;
}


int8_t GOPH_page_print(GOPH_page_t* page)
{
	int32_t i;
	for (i=0; i<page->nlines; i++) {
		GOPH_line_print(page->lines + i);
	}
	return 0;
}


int8_t GOPH_page_writeToFile(GOPH_page_t* page, char* filename)
{
	if (page->type != '1') {
		return 1;
	}

	FILE* fp = fopen(filename, "w");

	char* buffer;
	int len = 0;
	int32_t i;
	//FIXME: O buffer não devia fazer frees a torto e a direito
	for (i=0; i<page->nlines; i++) {
		len = GOPH_line_toString(page->lines + i, &buffer);
		// O len devolve o tamanho do buffer inteiro, portanto é necesário
		// descontar o \0
		fwrite(buffer, 1, len-1, fp);
		free(buffer);
	}
	fclose(fp);
	return 0;
}


//////// LIST_STR /////////

int8_t GOPH_list_str_init(GOPH_list_str *list) {
	// Inicialização da estruta;
	list->list = NULL;
	list->len  = 0;
	list->cap  = 0;
	return 0;
}

//FIXME: Repair return codes
int8_t GOPH_list_str_free(GOPH_list_str *list) {
	//if (list->len == 0) return 0;
	if (list->list == NULL) return 0;
	int32_t i;
	for (i=0; i<list->len; i++) {
		free(list->list[i]);
	}
	free(list->list);
	return 0;
}

GOPH_list_str GOPH_list_str_new(){
	GOPH_list_str list;
	GOPH_list_str_init(&list);
	return list;
}

int8_t GOPH_list_str_reset(GOPH_list_str *list) {
	GOPH_list_str_free(list);
	GOPH_list_str_init(list);
	return 0;
}


int8_t GOPH_list_str_push(GOPH_list_str *list, char* str, int32_t str_len)
{	
	const size_t cap_delta = 16;

	// Resize pointer list if necessary
	if (list->len == list->cap){
		list->list = realloc(list->list,
		                     (list->cap + cap_delta)*sizeof(char*));
		list->cap += cap_delta;
	}
	
	// Add new item
	list->list[list->len] = calloc(str_len + 1, 1);
	list->list[list->len][str_len] = 0;
	strncpy(list->list[list->len], str, str_len);
	
	list->len += 1;

	return 0;
}


int8_t GOPH_list_str_pop(GOPH_list_str *list)
{	
	const int32_t cap_delta = 16;

	if(list->len == 0) return 1;

	// Remove item
	free((list->list)[list->len - 1]);
	list->len -= 1;

	// Resize pointer list if necessary
	if (list->len == (list->cap - cap_delta)){
		list->list = realloc(list->list,
		                     (list->cap - cap_delta)*sizeof(char*));
		list->cap -= cap_delta;
	}

	return 0;
}



//////// USR ////////

int8_t GOPH_usr_init(GOPH_usr_t *usr)
{
	usr->sIndx    = 0;
	usr->sPort    = 0;
	usr->sDom     = NULL;
	usr->sSel     = NULL;
	usr->sType    = 0;
	usr->sockid   = 0;
	memset(&(usr->struAddr), 0, sizeof(usr->struAddr));

	return 0;
}


GOPH_usr_t GOPH_usr_new()
{
	GOPH_usr_t usr;
	GOPH_usr_init(&usr);
	return usr;
}
	

int8_t GOPH_usr_free(GOPH_usr_t* usr)
{
	free(usr->sDom);
	free(usr->sSel);
	return 0;
}


int8_t GOPH_usr_reset(GOPH_usr_t* usr)
{
	GOPH_usr_free(usr);
	GOPH_usr_init(usr);
	return 0;
}
