#ifndef _TINYSTATE_H_
#define _TINYSTATE_H_

#include "tinytypes.h"


typedef struct GOPH_state_t {
	GOPH_line_t page_line;
	int32_t     screen_top;
	int32_t     link_n;
} GOPH_state_t;

int8_t GOPH_state_init(GOPH_state_t* state);

int8_t GOPH_state_free(GOPH_state_t* state);

GOPH_state_t GOPH_state_new();

GOPH_state_t GOPH_state_clone(GOPH_state_t* state);

int8_t GOPH_state_reset(GOPH_state_t* state);


#endif
