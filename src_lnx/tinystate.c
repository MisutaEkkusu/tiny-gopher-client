#include "tinystate.h"


int8_t GOPH_state_init(GOPH_state_t* state)
{
	state->page_line  = GOPH_line_new();
	state->screen_top = 0;
	state->link_n     = -1;
	return 0;
}


int8_t GOPH_state_free(GOPH_state_t* state)
{
	GOPH_line_free(&(state->page_line));
	return 0;
}

int8_t GOPH_state_reset(GOPH_state_t* state)
{
	GOPH_state_free(state);
	GOPH_state_init(state);
	return 0;
}

GOPH_state_t GOPH_state_new()
{
	GOPH_state_t state;
	GOPH_state_init(&state);
	return state;
}

GOPH_state_t GOPH_state_clone(GOPH_state_t* state)
{
	GOPH_state_t clone;
	clone.page_line  = GOPH_line_clone(&(state->page_line));
	clone.screen_top = state->screen_top;
	clone.link_n     = state->link_n;
	return clone;
}
