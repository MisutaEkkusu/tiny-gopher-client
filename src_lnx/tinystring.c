// V1 (2018-10-28)

#include "tinystring.h"


void say(char* msg){
	fprintf(stderr, "%s\n", msg);
	fflush(stderr);
}

// Converts a input string array into an allocated pointer
char* GOPH_str_new(char* str)
{
	size_t len = strlen(str) + 1;
	char* new_str = malloc(len);
	strcpy(new_str, str);
	return new_str;
}


/*
// Um número não chega a ter 32 chars.
// Penso que o máximo sejam 24 chars para um double:
// 1 (sinal) + 1 (inteiro) + 1 (ponto) + 16 (decimal) + 1 (e) +
// 1 (sinal expoente) + 3 (expoente)
// Um int32_t tem no máximo 11 chars
// Portanto, um int8_t server para conter o número de chars
// Na verdade, o output deveria ser em size_t
int8_t GOPH_calc_int_strlen(int32_t num)
{
	
}
*/

size_t GOPH_str_i32_len(int32_t num)
{
	const int32_t len[4] = {10, 100, 10000, 100000000};
	int32_t digits = 1;
	if (num < 0) {
		digits += 1;
	}
	int32_t t = 0;
	for (int32_t i=3; i>=0; i--) {
		t = num/len[i];
		if (t!=0) {
			digits += (1<<i);
			num = t;
		}
	}
	return digits;
}

