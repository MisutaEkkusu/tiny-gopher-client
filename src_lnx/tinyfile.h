#ifndef _TINYUTILS_H
#define _TINYUTILS_H

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

#include "tinytypes.h"

// Reads a file into a GOPH_page_t
int8_t GOPH_get_file(char* filename, GOPH_page_t* page);

// Reads a text file
int8_t GOPH_get_file_txt(char* filename, GOPH_page_t* page);

#endif
