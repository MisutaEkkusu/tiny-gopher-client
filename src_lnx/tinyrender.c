#include "tinyrender.h"

/*
Generates a page with information about the current page.

*/
int8_t GOPH_render_pageInfo(GOPH_page_t* pout, GOPH_line_t* line)
{
	GOPH_page_reset(pout);
	
	// New page_t for file
	pout->type = '0';
	
	// Allocate buffer
	//TODO Calculate space
	size_t bsize = 4096;
	char* buffer = malloc(sizeof(char) * bsize);
	int n = 0;
	n = sprintf(buffer, "gopher://%s/%c%s\n", line->dom, line->type, line->sel);
	n += sprintf(buffer + n, "type: %c\n", line->type);
	n += sprintf(buffer + n, "disp: %s\n", line->disp);
	n += sprintf(buffer + n, "sel : %s\n", line->sel);
	n += sprintf(buffer + n, "dom : %s\n", line->dom);
	n += sprintf(buffer + n, "port: %u\n", line->port);

	pout->buffer_size = bsize;
	pout->buffer = buffer;
	
	return 0;
}
