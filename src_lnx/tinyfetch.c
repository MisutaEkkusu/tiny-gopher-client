#include "tinyfetch.h"


// Generates an error page
int8_t GOPH_fetch_makeErrorPage(GOPH_page_t* page, char* message)
{
	page->type        = '0';
	page->buffer_size = strlen(message) + 1;
	page->buffer      = GOPH_str_new(message);
	return 0;
}


int8_t GOPH_fetch_setupConnection(GOPH_usr_t* user)
{
	// Setup connection
	GOPH_SetupSockAddrIn(user->sPort,AF_INET, &(user->struAddr));
	if(GOPH_ResolveURL(user->sDom, &(user->struAddr))) {
		return 1;
	}
	if(GOPH_Connect_nonblock(user->struAddr, &(user->sockid))) {
		return 1;
	}
	
	send(user->sockid, user->sSel, strlen(user->sSel), 0);
	return 0;
}


//FIXME: Verificar se o "type" da página de saída tem de ser o mesmo declarado
//       no user.
//       - Talvez tenha de criar um novo tipo (ERRO)
int8_t GOPH_fetch_page(GOPH_usr_t* user, GOPH_page_t* page)
{
	GOPH_draw_textLineWithBG(-2, 1, " (connecting) ", "▚");

	if(GOPH_fetch_setupConnection(user)) {
		GOPH_fetch_makeErrorPage(page, "CONNECTION ERROR\n"); 
		return 1;
	}
	

	size_t buffer_size = 0;
	char*  buffer      = NULL;

	//FIXME Faltam códigos de erro decentes
	char dl_msg[64];
	while (1){
		sprintf(dl_msg, " %.1f KiB ", buffer_size/1024.0);
		GOPH_draw_textLineWithBG(-2, 1, dl_msg, "▚");
		if(GOPH_GetFile_loop(user->sockid, &buffer_size, &buffer)) {
			break;
		}
	}
	
	/*
	if(GOPH_GetFile_nonblock(user->sockid, &buffer_size, &buffer)) {
		GOPH_fetch_makeErrorPage(page, "DOWNLOAD ERROR\n"); 
		return 1;
	};
	*/

	// page construction
	switch(user->sType){
		
		case '0':
			page->type        = '0';
			page->buffer_size = buffer_size;
			page->buffer      = buffer;
			break;
			
		case '1':
			GOPH_parse_dir(buffer_size, buffer, page);
			free(buffer);
			break;
	}

	return 0;

}


// USER FUNCTIONS

int8_t GOPHER_fetch_setUserSelect(GOPH_usr_t* user, GOPH_line_t* line_next)
{
	size_t sel_size, dom_size;
	
	free(user->sDom);
	free(user->sSel);
	user->sDom = NULL;
	user->sSel = NULL;
	
	sel_size = strlen(line_next->sel);
	dom_size = strlen(line_next->dom);
	
	// Os números extra servem para escrever os caracteres finais.
	user->sDom = malloc((dom_size+1)*sizeof(char));
	user->sSel = malloc((sel_size+2)*sizeof(char));
	
	if(user->sSel == NULL || user->sDom == NULL) {
		return 1;
	}
		
	strncpy(user->sDom, line_next->dom, dom_size);
	user->sDom[dom_size] = '\0';
	strncpy(user->sSel, line_next->sel, sel_size);
	user->sSel[sel_size] = '\n';
	user->sSel[sel_size+1] = '\0';
	user->sPort = line_next->port;
	user->sType = line_next->type;
	
	return 0;
}
